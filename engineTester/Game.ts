import GameObject from "../renderEngine/GameObject";
import Vector3f from "../renderEngine/Vector3f";
import MainComponent from "./MainComponent";
import Time from "../renderEngine/Time";
import RenderUtil from "../utils/RenderUtil";
import DirectionalLight from "../renderEngine/DirectionalLight";
import Camera3D from "../renderEngine/Camera3D";
import GameWindow from "../renderEngine/GameWindow";
import ResourceLoader from "../loaders/ResourceLoader";
import { ShaderType } from "../shaders/Shader";
import BaseLight from "../renderEngine/BaseLight";
import Input from "../input/Input";
import PhongShader from "../shaders/PhongShader";

/**
 * Game class
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */

export default class Game {

    private directionalLight: DirectionalLight;
    private camera : Camera3D;

    private gameObject : GameObject;

    private ready : boolean = false;

    constructor() {

        this.gameObject = ResourceLoader.createGameObject({
            mesh: "cube.obj",
            shader: {
                type: ShaderType.PHONG
            },
            material: {
                texture: "cube.png",
                color: new Vector3f(1,1,1)
            }
        }, true);

        this.camera = new Camera3D();
        this.camera.setPos(new Vector3f(0,0,2));
        
        this.directionalLight = new DirectionalLight(
            new BaseLight(new Vector3f(1,1,1), 1),
            new Vector3f(1,1,1)
        );

        const shader: PhongShader = this.gameObject.getShader() as PhongShader;
        shader.setAmbientLight(new Vector3f(0.1,0.1,0.1));
        shader.setDirectionalLight(this.directionalLight);

        this.ready = true;
    }

    public input(): void {

        if(!this.ready) return;

        this.camera.input();
    }

    //temp:
    private temp: number = 0.0;
    public update(): void {

        if(!this.ready) return;

        this.temp += Time.getDelta();
        Input.update();
        this.gameObject.update(this.temp);
    }

    public render(): void {

        if(!this.ready) return;

        RenderUtil.setClearColor(new Vector3f(0.2,0.2,0.2));
        
        this.gameObject.render(this.camera, this.directionalLight);
    }
}
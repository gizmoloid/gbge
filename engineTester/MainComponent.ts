import GameWindow from "../renderEngine/GameWindow";
import RenderUtil from "../utils/RenderUtil";
import Display from "../renderEngine/Display";
import MathUtil from "../utils/MathUtil";
import Time from "../renderEngine/Time";
import Game from "./Game";
import ResourceLoader from "../loaders/ResourceLoader";
import Mouse from "../input/Mouse";
import Input from "../input/Input";
import Keyboard from "../input/Keyboard";

/**
 * Class for the main component
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */

export default class MainComponent {

    public static WIDTH: number = 800;
    public static HEIGHT: number = 600;
    public static TITLE: string = "Gizmo3D Game Engine";
    public static FRAME_CAP: number = 60;

    public isRunning: boolean = false;

    private game: Game;

    private fpsUI: HTMLElement;

    private callback: Function | null;

    constructor(callback:Function | null) {

        this.callback = callback;

        this.fpsUI = MathUtil.createFPSCounterElement();
        GameWindow.createWindow(MainComponent.WIDTH, MainComponent.HEIGHT, MainComponent.TITLE);
        RenderUtil.initGraphics(Display.canvas);
        this.isRunning = false;

        const instance = this;
        document.addEventListener("pointerlockchange", function(e) {
            let lock: boolean = document.pointerLockElement === Display.canvas;
            // Mouse.setMouseLock(document.pointerLockElement === Display.canvas);
            if(!lock) {
                instance.stop();
            }
        }, false);

        MathUtil.loadJSONFile(ResourceLoader.URL + "resources.json")
        .then((result) => {
            return ResourceLoader.loadResources(result);
        })
        .then(loaded => {
            this.game = new Game();
        });
    }

    public start(): void {

        if(this.isRunning){
            return;
        };
        this.run();
        this.isRunning = true;
        Mouse.setMouseLock(true);
        Display.canvas.requestPointerLock();
    }

    public run(): void {

        let instance: MainComponent = this;

        const frameTime: number = 1.0 / MainComponent.FRAME_CAP;

        let lastTime: number = Time.getTime();
        let unprocessedTime: number = 0;

        let frames: number = 0;
        let frameCounter: number = 0;

        let loop: Function = function(e) {

            let render: boolean = false;

            let startTime: number = Time.getTime();
            let passedTime: number = startTime - lastTime;
            lastTime = startTime;

            unprocessedTime += (passedTime / Time.SECOND);
            frameCounter += passedTime;

            while(unprocessedTime > frameTime) {

                render = true;
                unprocessedTime -= frameTime;

                if(!instance.isRunning) {
                    window.clearInterval(interID);
                }

                Time.setDelta(frameTime);
                
                instance.game.input();
                instance.game.update();

                if(Input.getKey(Keyboard.ESCAPE)) {
                    instance.stop();
                }

                if(frameCounter >= Time.SECOND) {

                    instance.fpsUI.innerHTML = "FPS: " + frames;
                    frames = 0;
                    frameCounter = 0;
                }
            }

            if(render) {
                instance.render();
                frames++;
            }
        }

        let interID: number = window.setInterval(loop, 1)
    }

    public stop(): void {

        if(!this.isRunning) {
            return;
        }
        this.isRunning = false;
        Mouse.setMouseLock(false);
        
        if(this.callback) {
            this.callback();
        }
    }
    
    private render(): void {

        RenderUtil.clearScreen();

        this.game.render();
        GameWindow.render();
    }
    
    private cleanUp(): void {
        GameWindow.dispose();
    }

    private onMouseLockChange(e): void {

        
    }
}
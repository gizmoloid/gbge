var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
define("utils/MathUtil", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class MathUtil {
        static toRadians(degrees) {
            return degrees * Math.PI / 180;
        }
        static createFPSCounterElement() {
            const res = document.createElement("p");
            res.style.position = "absolute";
            res.style.top = "0";
            res.style.left = "0";
            res.style.zIndex = "9999";
            res.style.padding = "0";
            res.style.margin = "0";
            document.body.appendChild(res);
            return res;
        }
        static indicesToInt32Array(indices) {
            return new Int32Array(indices);
            ;
        }
        static base64ArrayBuffer(arrayBuffer) {
            let base64 = "";
            const encodings = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
            const bytes = new Uint8Array(arrayBuffer);
            const byteLength = bytes.byteLength;
            const byteRemainder = byteLength % 3;
            const mainLength = byteLength - byteRemainder;
            let a;
            let b;
            let c;
            let d;
            let chunk;
            for (let i = 0; i < mainLength; i = i + 3) {
                chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];
                a = (chunk & 16515072) >> 18;
                b = (chunk & 258048) >> 12;
                c = (chunk & 4032) >> 6;
                d = chunk & 63;
                base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
            }
            if (byteRemainder == 1) {
                chunk = bytes[mainLength];
                a = (chunk & 252) >> 2;
                b = (chunk & 3) << 4;
                base64 += encodings[a] + encodings[b] + "==";
            }
            else if (byteRemainder == 2) {
                chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];
                a = (chunk & 64512) >> 10;
                b = (chunk & 1008) >> 4;
                c = (chunk & 15) << 2;
                base64 += encodings[a] + encodings[b] + encodings[c] + '=';
            }
            return base64;
        }
        static removeEmptyStrings(data) {
            let result = new Array();
            for (let i = 0; i < data.length; i++) {
                if (data[i] !== "") {
                    result.push(data[i]);
                }
            }
            return result;
        }
        static loadJSONFile(url) {
            return __awaiter(this, void 0, void 0, function* () {
                let response = yield fetch(url);
                return yield response.json();
            });
        }
        static loadASCIIFile(url) {
            return __awaiter(this, void 0, void 0, function* () {
                let response = yield fetch(url);
                return yield response.json();
            });
        }
    }
    exports.default = MathUtil;
});
define("renderEngine/Vector2f", ["require", "exports", "utils/MathUtil"], function (require, exports, MathUtil_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Vector2f extends Object {
        constructor(x = 0, y = 0) {
            super();
            this.x = x;
            this.y = y;
            this.elements = new Float32Array(2);
            this.elements[0] = this.x;
            this.elements[1] = this.y;
        }
        length() {
            return Math.sqrt(this.x * this.x + this.y * this.y);
        }
        dot(r) {
            return this.x * r.getX() + this.y * r.getY();
        }
        normalized() {
            const length = this.length();
            return new Vector2f(this.x / length, this.y / length);
        }
        rotate(angle) {
            let rad = MathUtil_1.default.toRadians(angle);
            let cos = Math.cos(rad);
            let sin = Math.sin(rad);
            return new Vector2f(this.x * cos - this.y * sin, this.x * sin + this.y * cos);
        }
        addVec(r) {
            return new Vector2f(this.x + r.getX(), this.y + r.getY());
        }
        addNum(r) {
            return new Vector2f(this.x + r, this.y + r);
        }
        subVec(r) {
            return new Vector2f(this.x - r.getX(), this.y - r.getY());
        }
        subNum(r) {
            return new Vector2f(this.x - r, this.y - r);
        }
        mulVec(r) {
            return new Vector2f(this.x * r.getX(), this.y * r.getY());
        }
        mulNum(r) {
            return new Vector2f(this.x * r, this.y * r);
        }
        divVec(r) {
            return new Vector2f(this.x / r.getX(), this.y / r.getY());
        }
        divNum(r) {
            return new Vector2f(this.x / r, this.y / r);
        }
        equals(vec) {
            return vec.getX() === this.getX() && vec.getY() === this.getY();
        }
        getX() {
            return this.x;
        }
        getY() {
            return this.y;
        }
        setX(x) {
            this.x = x;
        }
        setY(y) {
            this.y = y;
        }
        toString() {
            return "(x: " + this.x + " y: " + this.y + ")";
        }
    }
    exports.default = Vector2f;
});
define("renderEngine/Quaternion", ["require", "exports", "utils/MathUtil"], function (require, exports, MathUtil_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Quaternion {
        constructor(x = 0, y = 0, z = 0, w = 0) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }
        length() {
            return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
        }
        normalize() {
            let length = this.length();
            this.x /= length;
            this.y /= length;
            this.z /= length;
            this.w /= length;
            return this;
        }
        conjugate() {
            return new Quaternion(-this.x, -this.y, -this.z, this.w);
        }
        mulQuat(r) {
            let _w = this.w * r.getW() - this.x * r.getX() - this.y * r.getY() - this.z * r.getZ();
            let _x = this.x * r.getW() + this.w * r.getX() + this.y * r.getZ() - this.z * r.getY();
            let _y = this.y * r.getW() + this.w * r.getY() + this.z * r.getX() - this.x * r.getZ();
            let _z = this.z * r.getW() + this.w * r.getZ() + this.x * r.getY() - this.y * r.getX();
            return new Quaternion(_x, _y, _z, _w);
        }
        mulVec(r) {
            let w_ = -this.x * r.getX() - this.y * r.getY() - this.z * r.getZ();
            let x_ = this.w * r.getX() + this.y * r.getZ() - this.z * r.getY();
            let y_ = this.w * r.getY() + this.z * r.getX() - this.x * r.getZ();
            let z_ = this.w * r.getZ() + this.x * r.getY() - this.y * r.getX();
            return new Quaternion(x_, y_, z_, w_);
        }
        rotationAxisToQuaternion(angle, axis) {
            const angle_1 = MathUtil_2.default.toRadians(angle);
            const angle_2 = angle_1 * 0.5;
            const sine = Math.sin(angle_2);
            this.w = Math.cos(angle_2);
            this.x = axis.getX() * sine;
            this.y = axis.getY() * sine;
            this.z = axis.getZ() * sine;
        }
        getX() {
            return this.x;
        }
        getY() {
            return this.y;
        }
        getZ() {
            return this.z;
        }
        getW() {
            return this.w;
        }
        setX(x) {
            this.x = x;
        }
        setY(y) {
            this.y = y;
        }
        setZ(z) {
            this.z = z;
        }
        setW(w) {
            this.w = w;
        }
    }
    exports.default = Quaternion;
});
define("renderEngine/Vector3f", ["require", "exports", "utils/MathUtil", "renderEngine/Quaternion"], function (require, exports, MathUtil_3, Quaternion_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Vector3f {
        constructor(x = 0, y = 0, z = 0) {
            this.elements = new Float32Array(3);
            this.elements[0] = x;
            this.elements[1] = y;
            this.elements[2] = z;
        }
        addVec(vec) {
            return new Vector3f(this.elements[0] + vec.getX(), this.elements[1] + vec.getY(), this.elements[2] + vec.getZ());
        }
        subVec(vec) {
            return new Vector3f(this.getX() - vec.getX(), this.getY() - vec.getY(), this.getZ() - vec.getZ());
        }
        copy() {
            return new Vector3f(this.elements[0], this.elements[1], this.elements[2]);
        }
        mulNum(num) {
            return new Vector3f(this.elements[0] * num, this.elements[1] * num, this.elements[2] * num);
        }
        divVec(r) {
            return new Vector3f(this.elements[0] / r.getX(), this.elements[1] / r.getY(), this.elements[2] / r.getZ());
        }
        divNum(r) {
            return new Vector3f(this.elements[0] / r, this.elements[1] / r, this.elements[2] / r);
        }
        abs() {
            return new Vector3f(Math.abs(this.elements[0]), Math.abs(this.elements[1]), Math.abs(this.elements[2]));
        }
        cross(r) {
            const e = this.elements;
            const x_ = e[1] * r.getZ() - e[2] * r.getY();
            const y_ = e[2] * r.getX() - e[0] * r.getZ();
            const z_ = e[0] * r.getY() - e[1] * r.getX();
            return new Vector3f(x_, y_, z_);
        }
        rotate(angle, axis) {
            const sinHalfAngle = Math.sin(MathUtil_3.default.toRadians(angle / 2));
            const cosHalfAngle = Math.cos(MathUtil_3.default.toRadians(angle / 2));
            const rX = axis.getX() * sinHalfAngle;
            const rY = axis.getY() * sinHalfAngle;
            const rZ = axis.getZ() * sinHalfAngle;
            const rW = cosHalfAngle;
            const rotation = new Quaternion_1.default(rX, rY, rZ, rW);
            const conjugate = rotation.conjugate();
            const w = rotation.mulVec(this).mulQuat(conjugate);
            this.elements[0] = w.getX();
            this.elements[1] = w.getY();
            this.elements[2] = w.getZ();
            return this;
        }
        normalized() {
            const length = this.length();
            const v = this.elements;
            return new Vector3f(v[0] / length, v[1] / length, v[2] / length);
        }
        negate() {
            const v = this.elements;
            return new Vector3f(-v[0], -v[1], -v[2]);
        }
        length() {
            let v = this.elements;
            return (Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]));
        }
        getX() {
            return this.elements[0];
        }
        setX(x) {
            this.elements[0] = x;
        }
        getY() {
            return this.elements[1];
        }
        setY(y) {
            this.elements[1] = y;
        }
        getZ() {
            return this.elements[2];
        }
        setZ(z) {
            this.elements[2] = z;
        }
        static up() {
            return new Vector3f(0, 1, 0);
        }
    }
    exports.default = Vector3f;
});
define("utils/RenderUtil", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class RenderUtil {
        static clearScreen() {
            this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
        }
        static setClearColor(color) {
            this.gl.clearColor(color.getX(), color.getY(), color.getZ(), 1.0);
        }
        static getGLVersion() {
            return this.gl.getParameter(this.gl.VERSION);
        }
        static unbindTextures() {
            this.gl.bindTexture(this.gl.TEXTURE_2D, 0);
        }
        static initGraphics(canvas) {
            try {
                this.gl = canvas.getContext("webgl2");
            }
            catch (error) {
                throw new Error("WebGL error: " + error);
            }
            ;
            this.gl.clearColor(0, 0, 0, 0);
            this.gl.frontFace(this.gl.CCW);
            this.gl.cullFace(this.gl.BACK);
            this.gl.enable(this.gl.CULL_FACE);
            this.gl.enable(this.gl.DEPTH_TEST);
        }
    }
    exports.default = RenderUtil;
});
define("renderEngine/Mesh", ["require", "exports", "utils/RenderUtil"], function (require, exports, RenderUtil_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Mesh {
        constructor() {
            this.indexCount = 0;
        }
        createMeshVAO(name, indices, verts, normals, UVs) {
            this.indices = indices;
            this.vertices = verts;
            this.uvs = UVs;
            this.normals = normals;
        }
        draw() {
            const gl = RenderUtil_1.default.gl;
            gl.bindVertexArray(this.vao);
            gl.drawElementsInstanced(gl.TRIANGLES, this.indexCount, gl.UNSIGNED_SHORT, 0, 1);
        }
        setShader(program) {
            const gl = RenderUtil_1.default.gl;
            const vertexAttributeLocation = gl.getAttribLocation(program, "a_position");
            const texCoordAttributeLocation = gl.getAttribLocation(program, "a_texcoord");
            const normalAttributeLocation = gl.getAttribLocation(program, "a_normal");
            this.ibo = gl.createBuffer();
            this.indexCount = this.indices.length;
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibo);
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, this.indices, gl.STATIC_DRAW);
            this.vbo = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
            gl.bufferData(gl.ARRAY_BUFFER, this.vertices, gl.STATIC_DRAW);
            gl.enableVertexAttribArray(vertexAttributeLocation);
            gl.vertexAttribPointer(vertexAttributeLocation, 3, gl.FLOAT, false, 0, 0);
            this.UVbo = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.UVbo);
            gl.bufferData(gl.ARRAY_BUFFER, this.uvs, gl.STATIC_DRAW);
            gl.enableVertexAttribArray(texCoordAttributeLocation);
            gl.vertexAttribPointer(texCoordAttributeLocation, 2, gl.FLOAT, true, 0, 0);
            if (normalAttributeLocation != -1) {
                this.nbo = gl.createBuffer();
                gl.bindBuffer(gl.ARRAY_BUFFER, this.nbo);
                gl.bufferData(gl.ARRAY_BUFFER, this.normals, gl.STATIC_DRAW);
                gl.enableVertexAttribArray(normalAttributeLocation);
                gl.vertexAttribPointer(normalAttributeLocation, 3, gl.FLOAT, false, 0, 0);
            }
            this.vao = gl.createVertexArray();
            gl.bindVertexArray(this.vao);
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
            gl.enableVertexAttribArray(vertexAttributeLocation);
            gl.vertexAttribPointer(vertexAttributeLocation, 3, gl.FLOAT, false, 0, 0);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
            gl.bindBuffer(gl.ARRAY_BUFFER, this.UVbo);
            gl.enableVertexAttribArray(texCoordAttributeLocation);
            gl.vertexAttribPointer(texCoordAttributeLocation, 2, gl.FLOAT, true, 0, 0);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
            if (this.nbo) {
                gl.bindBuffer(gl.ARRAY_BUFFER, this.nbo);
                gl.enableVertexAttribArray(normalAttributeLocation);
                gl.vertexAttribPointer(normalAttributeLocation, 3, gl.HALF_FLOAT, false, 0, 0);
                gl.bindBuffer(gl.ARRAY_BUFFER, null);
            }
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibo);
            gl.bindVertexArray(null);
        }
    }
    exports.default = Mesh;
});
define("renderEngine/Matrix4f", ["require", "exports", "renderEngine/Vector3f"], function (require, exports, Vector3f_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Matrix4f {
        constructor() {
            this.elements = new Float32Array([1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1]);
        }
        setIdentity() {
            let e = this.elements;
            e[0] = 1;
            e[4] = 0;
            e[8] = 0;
            e[12] = 0;
            e[1] = 0;
            e[5] = 1;
            e[9] = 0;
            e[13] = 0;
            e[2] = 0;
            e[6] = 0;
            e[10] = 1;
            e[14] = 0;
            e[3] = 0;
            e[7] = 0;
            e[11] = 0;
            e[15] = 1;
            return this;
        }
        initCamera(forward, up) {
            let f = forward;
            f = f.normalized();
            let r = up;
            r = r.normalized();
            r = r.cross(f);
            let u = f.cross(r);
            let e = this.elements;
            e[0] = r.getX();
            e[4] = r.getY();
            e[8] = r.getZ();
            e[12] = 0;
            e[1] = u.getX();
            e[5] = u.getY();
            e[9] = u.getZ();
            e[13] = 0;
            e[2] = f.getX();
            e[6] = f.getY();
            e[10] = f.getZ();
            e[14] = 0;
            e[3] = 0;
            e[7] = 0;
            e[11] = 0;
            e[15] = 1;
            return this;
        }
        set(src) {
            let i;
            let s;
            let d;
            s = src.elements;
            d = this.elements;
            if (s === d) {
                return this;
            }
            for (i = 0; i < 16; ++i) {
                d[i] = s[i];
            }
            return this;
        }
        concat(other) {
            let i;
            let e = this.elements;
            let a = this.elements;
            let b = other.elements;
            let ai0;
            let ai1;
            let ai2;
            let ai3;
            if (e === b) {
                b = new Float32Array(16);
                for (i = 0; i < 16; ++i) {
                    b[i] = e[i];
                }
            }
            for (i = 0; i < 4; i++) {
                ai0 = a[i];
                ai1 = a[i + 4];
                ai2 = a[i + 8];
                ai3 = a[i + 12];
                e[i] = ai0 * b[0] + ai1 * b[1] + ai2 * b[2] + ai3 * b[3];
                e[i + 4] = ai0 * b[4] + ai1 * b[5] + ai2 * b[6] + ai3 * b[7];
                e[i + 8] = ai0 * b[8] + ai1 * b[9] + ai2 * b[10] + ai3 * b[11];
                e[i + 12] = ai0 * b[12] + ai1 * b[13] + ai2 * b[14] + ai3 * b[15];
            }
            return this;
        }
        multipy(other) {
            return this.concat(other);
        }
        multiplyVector3f(pos) {
            const e = this.elements;
            const p = pos.elements;
            const v = new Vector3f_1.default();
            let result = v.elements;
            result[0] = p[0] * e[0] + p[1] * e[4] + p[2] * e[8] + p[3] * e[12];
            result[1] = p[0] * e[1] + p[1] * e[5] + p[2] * e[9] + p[3] * e[13];
            result[2] = p[0] * e[2] + p[1] * e[6] + p[2] * e[10] + p[3] * e[14];
            result[3] = p[0] * e[3] + p[1] * e[7] + p[2] * e[11] + p[3] * e[15];
            return v;
        }
        transpose() {
            let e;
            let t;
            e = this.elements;
            t = e[1];
            e[1] = e[4];
            e[4] = t;
            t = e[2];
            e[2] = e[8];
            e[8] = t;
            t = e[3];
            e[3] = e[12];
            e[12] = t;
            t = e[6];
            e[6] = e[9];
            e[9] = t;
            t = e[7];
            e[7] = e[13];
            e[13] = t;
            t = e[11];
            e[11] = e[14];
            e[14] = t;
            return this;
        }
        ;
        setInverseOf(other) {
            let i;
            let s;
            let d;
            let inv;
            let det;
            s = other.elements;
            d = this.elements;
            inv = new Float32Array(16);
            inv[0] = s[5] * s[10] * s[15] - s[5] * s[11] * s[14] - s[9] * s[6] * s[15]
                + s[9] * s[7] * s[14] + s[13] * s[6] * s[11] - s[13] * s[7] * s[10];
            inv[4] = -s[4] * s[10] * s[15] + s[4] * s[11] * s[14] + s[8] * s[6] * s[15]
                - s[8] * s[7] * s[14] - s[12] * s[6] * s[11] + s[12] * s[7] * s[10];
            inv[8] = s[4] * s[9] * s[15] - s[4] * s[11] * s[13] - s[8] * s[5] * s[15]
                + s[8] * s[7] * s[13] + s[12] * s[5] * s[11] - s[12] * s[7] * s[9];
            inv[12] = -s[4] * s[9] * s[14] + s[4] * s[10] * s[13] + s[8] * s[5] * s[14]
                - s[8] * s[6] * s[13] - s[12] * s[5] * s[10] + s[12] * s[6] * s[9];
            inv[1] = -s[1] * s[10] * s[15] + s[1] * s[11] * s[14] + s[9] * s[2] * s[15]
                - s[9] * s[3] * s[14] - s[13] * s[2] * s[11] + s[13] * s[3] * s[10];
            inv[5] = s[0] * s[10] * s[15] - s[0] * s[11] * s[14] - s[8] * s[2] * s[15]
                + s[8] * s[3] * s[14] + s[12] * s[2] * s[11] - s[12] * s[3] * s[10];
            inv[9] = -s[0] * s[9] * s[15] + s[0] * s[11] * s[13] + s[8] * s[1] * s[15]
                - s[8] * s[3] * s[13] - s[12] * s[1] * s[11] + s[12] * s[3] * s[9];
            inv[13] = s[0] * s[9] * s[14] - s[0] * s[10] * s[13] - s[8] * s[1] * s[14]
                + s[8] * s[2] * s[13] + s[12] * s[1] * s[10] - s[12] * s[2] * s[9];
            inv[2] = s[1] * s[6] * s[15] - s[1] * s[7] * s[14] - s[5] * s[2] * s[15]
                + s[5] * s[3] * s[14] + s[13] * s[2] * s[7] - s[13] * s[3] * s[6];
            inv[6] = -s[0] * s[6] * s[15] + s[0] * s[7] * s[14] + s[4] * s[2] * s[15]
                - s[4] * s[3] * s[14] - s[12] * s[2] * s[7] + s[12] * s[3] * s[6];
            inv[10] = s[0] * s[5] * s[15] - s[0] * s[7] * s[13] - s[4] * s[1] * s[15]
                + s[4] * s[3] * s[13] + s[12] * s[1] * s[7] - s[12] * s[3] * s[5];
            inv[14] = -s[0] * s[5] * s[14] + s[0] * s[6] * s[13] + s[4] * s[1] * s[14]
                - s[4] * s[2] * s[13] - s[12] * s[1] * s[6] + s[12] * s[2] * s[5];
            inv[3] = -s[1] * s[6] * s[11] + s[1] * s[7] * s[10] + s[5] * s[2] * s[11]
                - s[5] * s[3] * s[10] - s[9] * s[2] * s[7] + s[9] * s[3] * s[6];
            inv[7] = s[0] * s[6] * s[11] - s[0] * s[7] * s[10] - s[4] * s[2] * s[11]
                + s[4] * s[3] * s[10] + s[8] * s[2] * s[7] - s[8] * s[3] * s[6];
            inv[11] = -s[0] * s[5] * s[11] + s[0] * s[7] * s[9] + s[4] * s[1] * s[11]
                - s[4] * s[3] * s[9] - s[8] * s[1] * s[7] + s[8] * s[3] * s[5];
            inv[15] = s[0] * s[5] * s[10] - s[0] * s[6] * s[9] - s[4] * s[1] * s[10]
                + s[4] * s[2] * s[9] + s[8] * s[1] * s[6] - s[8] * s[2] * s[5];
            det = s[0] * inv[0] + s[1] * inv[4] + s[2] * inv[8] + s[3] * inv[12];
            if (det === 0) {
                return this;
            }
            det = 1 / det;
            for (i = 0; i < 16; i++) {
                d[i] = inv[i] * det;
            }
            return this;
        }
        ;
        invert() {
            return this.setInverseOf(this);
        }
        ;
        setOrtho(left, right, bottom, top, near, far) {
            let e;
            let rw;
            let rh;
            let rd;
            if (left === right || bottom === top || near === far) {
                throw "Null Frustum!";
            }
            rw = 1 / (right - left);
            rh = 1 / (top - bottom);
            rd = 1 / (far - near);
            e = this.elements;
            e[0] = 2 * rw;
            e[1] = 0;
            e[2] = 0;
            e[3] = 0;
            e[4] = 0;
            e[5] = 2 * rh;
            e[6] = 0;
            e[7] = 0;
            e[8] = 0;
            e[9] = 0;
            e[10] = -2 * rd;
            e[11] = 0;
            e[12] = -(right + left) * rw;
            e[13] = -(top + bottom) * rh;
            e[14] = -(far + near) * rd;
            e[15] = 1;
            return this;
        }
        ;
        ortho(left, right, bottom, top, near, far) {
            return this.concat(new Matrix4f().setOrtho(left, right, bottom, top, near, far));
        }
        ;
        setFrustum(left, right, bottom, top, near, far) {
            let e;
            let rw;
            let rh;
            let rd;
            if (left === right || top === bottom || near === far) {
                throw "Null Frustum";
            }
            if (near <= 0) {
                throw "near <= 0";
            }
            if (far <= 0) {
                throw "far <= 0";
            }
            rw = 1 / (right - left);
            rh = 1 / (top - bottom);
            rd = 1 / (far - near);
            e = this.elements;
            e[0] = 2 * near * rw;
            e[1] = 0;
            e[2] = 0;
            e[3] = 0;
            e[4] = 0;
            e[5] = 2 * near * rh;
            e[6] = 0;
            e[7] = 0;
            e[8] = (right + left) * rw;
            e[9] = (top + bottom) * rh;
            e[10] = -(far + near) * rd;
            e[11] = -1;
            e[12] = 0;
            e[13] = 0;
            e[14] = -2 * near * far * rd;
            e[15] = 0;
            return this;
        }
        ;
        frustum(left, right, bottom, top, near, far) {
            return this.concat(new Matrix4f().setFrustum(left, right, bottom, top, near, far));
        }
        ;
        setPerspective(fovy, aspect, near, far) {
            let e;
            let rd;
            let s;
            let ct;
            if (near === far || aspect === 0) {
                throw "Null Frustum";
            }
            if (near <= 0) {
                throw "Near <= 0";
            }
            if (far <= 0) {
                throw "Far <= 0";
            }
            fovy = Math.PI * fovy / 180 / 2;
            s = Math.sin(fovy);
            if (s === 0) {
                throw "Null Frustum";
            }
            rd = 1 / (far - near);
            ct = Math.cos(fovy) / s;
            e = this.elements;
            e[0] = ct / aspect;
            e[1] = 0;
            e[2] = 0;
            e[3] = 0;
            e[4] = 0;
            e[5] = ct;
            e[6] = 0;
            e[7] = 0;
            e[8] = 0;
            e[9] = 0;
            e[10] = -(far + near) * rd;
            e[11] = -1;
            e[12] = 0;
            e[13] = 0;
            e[14] = -2 * near * far * rd;
            e[15] = 0;
            return this;
        }
        ;
        perspective(fovy, aspect, near, far) {
            return this.concat(new Matrix4f().setPerspective(fovy, aspect, near, far));
        }
        ;
        setScale(x, y, z) {
            let e = this.elements;
            e[0] = x;
            e[4] = 0;
            e[8] = 0;
            e[12] = 0;
            e[1] = 0;
            e[5] = y;
            e[9] = 0;
            e[13] = 0;
            e[2] = 0;
            e[6] = 0;
            e[10] = z;
            e[14] = 0;
            e[3] = 0;
            e[7] = 0;
            e[11] = 0;
            e[15] = 1;
            return this;
        }
        ;
        scale(x, y, z) {
            let e = this.elements;
            e[0] *= x;
            e[4] *= y;
            e[8] *= z;
            e[1] *= x;
            e[5] *= y;
            e[9] *= z;
            e[2] *= x;
            e[6] *= y;
            e[10] *= z;
            e[3] *= x;
            e[7] *= y;
            e[11] *= z;
            return this;
        }
        ;
        initTranslate(x, y, z) {
            let e = this.elements;
            e[0] = 1;
            e[4] = 0;
            e[8] = 0;
            e[12] = x;
            e[1] = 0;
            e[5] = 1;
            e[9] = 0;
            e[13] = y;
            e[2] = 0;
            e[6] = 0;
            e[10] = 1;
            e[14] = z;
            e[3] = 0;
            e[7] = 0;
            e[11] = 0;
            e[15] = 1;
            return this;
        }
        ;
        setTranslateVec(vec) {
            return this.initTranslate(vec.getX(), vec.getY(), vec.getZ());
        }
        ;
        translate(x, y, z) {
            let e = this.elements;
            e[12] += e[0] * x + e[4] * y + e[8] * z;
            e[13] += e[1] * x + e[5] * y + e[9] * z;
            e[14] += e[2] * x + e[6] * y + e[10] * z;
            e[15] += e[3] * x + e[7] * y + e[11] * z;
            return this;
        }
        ;
        translateVec(pos) {
            return this.translate(pos.getX(), pos.getY(), pos.getZ());
        }
        setRotate(angle, x, y, z) {
            let e = this.elements;
            let s;
            let c;
            let len;
            let rlen;
            let nc;
            let xy;
            let yz;
            let zx;
            let xs;
            let ys;
            let zs;
            angle = Math.PI * angle / 180;
            e = this.elements;
            s = Math.sin(angle);
            c = Math.cos(angle);
            if (0 !== x && 0 === y && 0 === z) {
                if (x < 0) {
                    s = -s;
                }
                e[0] = 1;
                e[4] = 0;
                e[8] = 0;
                e[12] = 0;
                e[1] = 0;
                e[5] = c;
                e[9] = -s;
                e[13] = 0;
                e[2] = 0;
                e[6] = s;
                e[10] = c;
                e[14] = 0;
                e[3] = 0;
                e[7] = 0;
                e[11] = 0;
                e[15] = 1;
            }
            else if (0 === x && 0 !== y && 0 === z) {
                if (y < 0) {
                    s = -s;
                }
                e[0] = c;
                e[4] = 0;
                e[8] = s;
                e[12] = 0;
                e[1] = 0;
                e[5] = 1;
                e[9] = 0;
                e[13] = 0;
                e[2] = -s;
                e[6] = 0;
                e[10] = c;
                e[14] = 0;
                e[3] = 0;
                e[7] = 0;
                e[11] = 0;
                e[15] = 1;
            }
            else if (0 === x && 0 === y && 0 !== z) {
                if (z < 0) {
                    s = -s;
                }
                e[0] = c;
                e[4] = -s;
                e[8] = 0;
                e[12] = 0;
                e[1] = s;
                e[5] = c;
                e[9] = 0;
                e[13] = 0;
                e[2] = 0;
                e[6] = 0;
                e[10] = 1;
                e[14] = 0;
                e[3] = 0;
                e[7] = 0;
                e[11] = 0;
                e[15] = 1;
            }
            else {
                len = Math.sqrt(x * x + y * y + z * z);
                if (len !== 1) {
                    rlen = 1 / len;
                    x *= rlen;
                    y *= rlen;
                    z *= rlen;
                }
                nc = 1 - c;
                xy = x * y;
                yz = y * z;
                zx = z * x;
                xs = x * s;
                ys = y * s;
                zs = z * s;
                e[0] = x * x * nc + c;
                e[1] = xy * nc + zs;
                e[2] = zx * nc - ys;
                e[3] = 0;
                e[4] = xy * nc - zs;
                e[5] = y * y * nc + c;
                e[6] = yz * nc + xs;
                e[7] = 0;
                e[8] = zx * nc + ys;
                e[9] = yz * nc - xs;
                e[10] = z * z * nc + c;
                e[11] = 0;
                e[12] = 0;
                e[13] = 0;
                e[14] = 0;
                e[15] = 1;
            }
            return this;
        }
        ;
        rotate(angle, x, y, z) {
            return this.concat(new Matrix4f().setRotate(angle, x, y, z));
        }
        ;
        setLookAt(eyePos, targetPos, up) {
            let e;
            let fx;
            let fy;
            let fz;
            let rlf;
            let sx;
            let sy;
            let sz;
            let rls;
            let ux;
            let uy;
            let uz;
            fx = targetPos.getX() - eyePos.getX();
            fy = targetPos.getY() - eyePos.getY();
            fz = targetPos.getZ() - eyePos.getZ();
            rlf = 1 / Math.sqrt(fx * fx + fy * fy + fz * fz);
            fx *= rlf;
            fy *= rlf;
            fz *= rlf;
            sx = fy * up.getZ() - fz * up.getY();
            sy = fz * up.getX() - fx * up.getZ();
            sz = fx * up.getY() - fy * up.getX();
            rls = 1 / Math.sqrt(sx * sx + sy * sy + sz * sz);
            sx *= rls;
            sy *= rls;
            sz *= rls;
            ux = sy * fz - sz * fy;
            uy = sz * fx - sx * fz;
            uz = sx * fy - sy * fx;
            e = this.elements;
            e[0] = sx;
            e[1] = ux;
            e[2] = -fx;
            e[3] = 0;
            e[4] = sy;
            e[5] = uy;
            e[6] = -fy;
            e[7] = 0;
            e[8] = sz;
            e[9] = uz;
            e[10] = -fz;
            e[11] = 0;
            e[12] = 0;
            e[13] = 0;
            e[14] = 0;
            e[15] = 1;
            return this.translate(-eyePos.getX(), -eyePos.getY(), -eyePos.getZ());
        }
        ;
        lookAt(eyePos, targetPos, up) {
            return this.concat(new Matrix4f().setLookAt(eyePos, targetPos, up));
        }
        ;
        dropShadow(plane, light) {
            let mat = new Matrix4f();
            let e = mat.elements;
            var dot = plane[0] * light[0] + plane[1] * light[1] + plane[2] * light[2] + plane[3] * light[3];
            e[0] = dot - light[0] * plane[0];
            e[1] = -light[1] * plane[0];
            e[2] = -light[2] * plane[0];
            e[3] = -light[3] * plane[0];
            e[4] = -light[0] * plane[1];
            e[5] = dot - light[1] * plane[1];
            e[6] = -light[2] * plane[1];
            e[7] = -light[3] * plane[1];
            e[8] = -light[0] * plane[2];
            e[9] = -light[1] * plane[2];
            e[10] = dot - light[2] * plane[2];
            e[11] = -light[3] * plane[2];
            e[12] = -light[0] * plane[3];
            e[13] = -light[1] * plane[3];
            e[14] = -light[2] * plane[3];
            e[15] = dot - light[3] * plane[3];
            return this.concat(mat);
        }
        dropShadowDirectionally(normX, normY, normZ, planeX, planeY, planeZ, lightX, lightY, lightZ) {
            let a = planeX * normX + planeY * normY + planeZ * normZ;
            return this.dropShadow([normX, normY, normZ, -a], [lightX, lightY, lightZ, 0]);
        }
    }
    exports.default = Matrix4f;
});
define("renderEngine/BaseLight", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class BaseLight {
        constructor(color, intensity) {
            this.color = color;
            this.intensity = intensity;
        }
        getColor() {
            return this.color;
        }
        setColor(color) {
            this.color = color;
        }
        getIntensity() {
            return this.intensity;
        }
        setIntensity(intensity) {
            this.intensity = intensity;
        }
    }
    exports.default = BaseLight;
});
define("shaders/Shader", ["require", "exports", "utils/RenderUtil"], function (require, exports, RenderUtil_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Shader {
        constructor() {
            this.program = RenderUtil_2.default.gl.createProgram();
            this.uniforms = new Map();
            if (RenderUtil_2.default.gl.getProgramParameter(this.program, RenderUtil_2.default.gl.LINK_STATUS)) {
                throw new Error("Error memory valid location not valid!");
            }
        }
        bind() {
            RenderUtil_2.default.gl.useProgram(this.program);
        }
        setShaders(vertexShader, fragShader) {
            const gl = RenderUtil_2.default.gl;
            this.addProgram(vertexShader, gl.VERTEX_SHADER);
            this.addProgram(fragShader, gl.FRAGMENT_SHADER);
            gl.linkProgram(this.program);
            if (!gl.getProgramParameter(this.program, gl.LINK_STATUS)) {
                throw new Error("Error compile program link status");
            }
            gl.validateProgram(this.program);
            if (!gl.getProgramParameter(this.program, gl.VALIDATE_STATUS)) {
                throw new Error("Error compile program validate status");
            }
        }
        addUniform(uniformName) {
            this.uniforms.set(uniformName, RenderUtil_2.default.gl.getUniformLocation(this.program, uniformName));
        }
        addProgram(source, type) {
            const gl = RenderUtil_2.default.gl;
            const shader = gl.createShader(type);
            gl.shaderSource(shader, source);
            gl.compileShader(shader);
            if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
                throw new Error("Error compiling shader program: \n" + source);
            }
            gl.attachShader(this.program, shader);
        }
        update(worldMatrix, mvp, material) {
            this.bind();
        }
        getProgram() {
            return this.program;
        }
        setUniformi(uniformName, value) {
            RenderUtil_2.default.gl.uniform1i(this.uniforms.get(uniformName), value);
        }
        setUniformf(uniformName, value) {
            RenderUtil_2.default.gl.uniform1f(this.uniforms.get(uniformName), value);
        }
        setUniformVec(uniformName, value) {
            RenderUtil_2.default.gl.uniform3f(this.uniforms.get(uniformName), value.getX(), value.getY(), value.getZ());
        }
        setUniform(uniformName, value) {
            const uniformN = this.uniforms.get(uniformName);
            RenderUtil_2.default.gl.uniformMatrix4fv(uniformN, false, value.elements);
        }
    }
    exports.default = Shader;
    var ShaderType;
    (function (ShaderType) {
        ShaderType["BASIC"] = "basic";
        ShaderType["PHONG"] = "phong";
        ShaderType["CUSTOM"] = "custom";
    })(ShaderType = exports.ShaderType || (exports.ShaderType = {}));
});
define("shaders/BasicShader", ["require", "exports", "shaders/Shader", "loaders/ResourceLoader", "utils/RenderUtil"], function (require, exports, Shader_1, ResourceLoader_1, RenderUtil_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class BasicShader extends Shader_1.default {
        constructor() {
            super();
            this.setShaders(ResourceLoader_1.default.loadShader("basicVertex.glsl"), ResourceLoader_1.default.loadShader("basicFragment.glsl"));
            this.addUniform("worldMatrix");
            this.addUniform("baseColor");
            this.addUniform("sampled");
        }
        update(worldMatrix, mvp, material) {
            super.update(worldMatrix, mvp, material);
            if (material.getTexture() !== null) {
                material.getTexture().bind();
            }
            else {
                RenderUtil_3.default.unbindTextures();
            }
            this.setUniform("worldMatrix", mvp);
            this.setUniformVec("baseColor", material.getColor());
        }
    }
    exports.default = BasicShader;
});
define("renderEngine/DirectionalLight", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class DirectionalLight {
        constructor(base, direction) {
            this.base = base;
            this.direction = direction.normalized();
        }
        getBase() {
            return this.base;
        }
        setBase(base) {
            this.base = base;
        }
        getDirection() {
            return this.direction;
        }
        setDirection(direction) {
            this.direction = direction;
        }
    }
    exports.default = DirectionalLight;
});
define("shaders/PhongShader", ["require", "exports", "shaders/Shader", "utils/RenderUtil", "renderEngine/Vector3f", "loaders/ResourceLoader", "renderEngine/DirectionalLight", "renderEngine/BaseLight"], function (require, exports, Shader_2, RenderUtil_4, Vector3f_2, ResourceLoader_2, DirectionalLight_1, BaseLight_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class PhongShader extends Shader_2.default {
        constructor() {
            super();
            this.ambientLight = new Vector3f_2.default(0.1, 0.1, 0.1);
            this.directionalLight = new DirectionalLight_1.default(new BaseLight_1.default(new Vector3f_2.default(1, 1, 1), 0), new Vector3f_2.default(0, 0, 0));
            this.setShaders(ResourceLoader_2.default.loadShader("phongVertex.glsl"), ResourceLoader_2.default.loadShader("phongFragment.glsl"));
            this.addUniform("worldMatrix");
            this.addUniform("mvp");
            this.addUniform("baseColor");
            this.addUniform("ambientLight");
            this.addUniform("directionalLight.base.color");
            this.addUniform("directionalLight.base.intensity");
            this.addUniform("directionalLight.direction");
        }
        update(worldMatrix, mvp, material) {
            super.update(worldMatrix, mvp, material);
            if (material.getTexture() !== null) {
                material.getTexture().bind();
            }
            else {
                RenderUtil_4.default.unbindTextures();
            }
            this.setUniform("mvp", mvp);
            this.setUniform("worldMatrix", worldMatrix);
            this.setUniformVec("baseColor", material.getColor());
            this.setUniformVec("ambientLight", this.ambientLight);
            this.setUniformDirLight("directionalLight", this.directionalLight);
        }
        setUniformBaseLight(uniformName, baseLight) {
            this.setUniformVec(uniformName + ".color", baseLight.getColor());
            this.setUniformf(uniformName + ".intensity", baseLight.getIntensity());
        }
        setUniformDirLight(uniformName, directionalLight) {
            this.setUniformBaseLight(uniformName + ".base", directionalLight.getBase());
            this.setUniformVec(uniformName + ".direction", directionalLight.getDirection());
        }
        getAmbientLight() {
            return this.ambientLight;
        }
        setAmbientLight(ambientLight) {
            this.ambientLight = ambientLight;
        }
        getDirectionalLight() {
            return this.directionalLight;
        }
        setDirectionalLight(directionalLight) {
            this.directionalLight = directionalLight;
        }
    }
    exports.default = PhongShader;
});
define("loaders/ResourceLoader", ["require", "exports", "renderEngine/Vector3f", "renderEngine/Vector2f", "utils/MathUtil", "renderEngine/Mesh", "renderEngine/GameObject", "renderEngine/Material", "renderEngine/Texture", "shaders/BasicShader", "shaders/Shader", "shaders/PhongShader"], function (require, exports, Vector3f_3, Vector2f_1, MathUtil_4, Mesh_1, GameObject_1, Material_1, Texture_1, BasicShader_1, Shader_3, PhongShader_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Formats;
    (function (Formats) {
        Formats["OBJ"] = "obj";
        Formats["FBX"] = "fbx";
        Formats["GLSL"] = "glsl";
    })(Formats || (Formats = {}));
    class ResourceLoader {
        static loadResources(json) {
            return __awaiter(this, void 0, void 0, function* () {
                let models = Array();
                let shaders = Array();
                let textures = Array();
                for (let i = 0; i < json.models.files.length; i++) {
                    models.push(json.models.path + json.models.files[i]);
                }
                for (let i = 0; i < json.shaders.files.length; i++) {
                    shaders.push(json.shaders.path + json.shaders.files[i]);
                }
                for (let i = 0; i < json.textures.files.length; i++) {
                    textures.push(json.textures.path + json.textures.files[i]);
                }
                return Promise.all(models.map(url => fetch(ResourceLoader.URL + url).then(resp => resp.text()))).then((mData) => {
                    let key;
                    for (let i = 0; i < mData.length; i++) {
                        key = models[i].split("/")[1];
                        ResourceLoader.modelsData.set(key, mData[i]);
                    }
                    return Promise.all(shaders.map(url => fetch(ResourceLoader.URL + url).then(resp => resp.text()))).then(sData => {
                        for (let i = 0; i < sData.length; i++) {
                            key = shaders[i].split("/")[1];
                            ResourceLoader.shadersData.set(key, sData[i]);
                        }
                        return Promise.all(textures.map(url => fetch(ResourceLoader.URL + url).then(resp => resp.blob()))).then(tData => {
                            for (let i = 0; i < tData.length; i++) {
                                key = textures[i].split("/")[1];
                                ResourceLoader.texturesData.set(key, textures[i]);
                            }
                            return true;
                        });
                    });
                }).catch((error) => {
                    return false;
                });
            });
        }
        static loadShader(shaderName) {
            const sData = ResourceLoader.shadersData.get(shaderName);
            if (!sData)
                throw new Error("No shader found: " + shaderName);
            return sData;
        }
        static loadMesh(meshName, calcNormals) {
            const arr = meshName.split(".");
            const fileFormat = arr[arr.length - 1];
            const mData = ResourceLoader.modelsData.get(meshName);
            if (!mData)
                throw new Error("No mesh found: " + meshName);
            switch (fileFormat) {
                case Formats.OBJ:
                    return ResourceLoader.parseOBJ(mData, calcNormals);
                case Formats.FBX:
                    return ResourceLoader.parseFBX(mData);
                default:
                    throw new Error("File format not available: " + meshName);
            }
        }
        static createGameObject(data, calcNormals) {
            const mesh = ResourceLoader.loadMesh(data.mesh, calcNormals);
            let shader;
            switch (data.shader.type) {
                case Shader_3.ShaderType.BASIC:
                    shader = new BasicShader_1.default();
                    break;
                case Shader_3.ShaderType.PHONG:
                    shader = new PhongShader_1.default();
                    break;
                case Shader_3.ShaderType.CUSTOM:
                    shader = new Shader_3.default();
                    shader.setShaders(ResourceLoader.loadShader(data.shader.vShader), ResourceLoader.loadShader(data.shader.fShader));
                    break;
                default:
                    shader = new Shader_3.default();
                    shader.setShaders(ResourceLoader.loadShader("basicVertex.glsl"), ResourceLoader.loadShader("basicFragment.glsl"));
            }
            mesh.setShader(shader.getProgram());
            const material = new Material_1.default(new Texture_1.default("textures/" + data.material.texture), data.material.color);
            return new GameObject_1.default(mesh, shader, material);
        }
        static loadTexture(textureName, callback) {
            const arr = textureName.split(".");
            const fileFormat = arr[arr.length - 1];
            let counter = 0;
            let texture;
            const totalTextures = ResourceLoader.texturesData.size;
            let textureURL;
            for (let i = 0; i < ResourceLoader.texturesData.size; i++) {
                textureURL = ResourceLoader.texturesData.get(textureName);
                if (textureURL)
                    return new Texture_1.default(textureURL);
            }
            return texture;
        }
        static parseFBX(fbxData) {
            let mesh = new Mesh_1.default();
            return mesh;
        }
        static parseOBJ(objData, calcNormals) {
            let mesh = new Mesh_1.default();
            let name = "no name";
            let vertices = new Array();
            let texCoords = new Array();
            let normals = new Array();
            let indices = new Array();
            let linesArray = objData.split("\n");
            let tokens;
            for (let i = 0; i < linesArray.length; i++) {
                tokens = linesArray[i].split(" ");
                tokens = MathUtil_4.default.removeEmptyStrings(tokens);
                switch (tokens[0]) {
                    case ResourceLoader.OBJProperties.GROUP:
                        name = tokens[1];
                        break;
                    case ResourceLoader.OBJProperties.VERTEX:
                        vertices.push(new Vector3f_3.default(parseFloat(tokens[1]), parseFloat(tokens[2]), parseFloat(tokens[3])));
                        break;
                    case ResourceLoader.OBJProperties.UV:
                        texCoords.push(new Vector2f_1.default(parseFloat(tokens[1]), parseFloat(tokens[2])));
                        break;
                    case ResourceLoader.OBJProperties.NORMAL:
                        normals.push(new Vector3f_3.default(parseFloat(tokens[1]), parseFloat(tokens[2]), parseFloat(tokens[3])));
                        break;
                    case ResourceLoader.OBJProperties.FACE:
                        indices.push(OBJIndex.parseIndex(tokens[1]));
                        indices.push(OBJIndex.parseIndex(tokens[2]));
                        indices.push(OBJIndex.parseIndex(tokens[3]));
                        break;
                    default:
                        continue;
                }
            }
            let positionsOut = new Array();
            let texCoordsOut = new Array();
            let normalsOut = new Array();
            let indicesOut = new Array();
            for (let i = 0; i < indices.length; i++) {
                let currentIndex = indices[i];
                let currentPosition = vertices[currentIndex.vertexIndex];
                positionsOut.push(currentPosition.getX());
                positionsOut.push(currentPosition.getY());
                positionsOut.push(currentPosition.getZ());
                let currentTexCoord;
                if (currentIndex.hasTexCoords) {
                    currentTexCoord = texCoords[currentIndex.texCoordIndex];
                }
                else {
                    currentTexCoord = new Vector2f_1.default(0, 0);
                    ;
                }
                texCoordsOut.push(currentTexCoord.getX());
                texCoordsOut.push(currentTexCoord.getY());
                let currentNormal;
                if (currentIndex.hasNormals) {
                    currentNormal = normals[currentIndex.normalIndex];
                }
                else {
                    currentNormal = new Vector3f_3.default(0, 0, 0);
                }
                currentNormal = currentNormal.normalized();
                normalsOut.push(currentNormal.getX());
                normalsOut.push(currentNormal.getY());
                normalsOut.push(currentNormal.getZ());
                indicesOut.push(i);
            }
            mesh.createMeshVAO(name, new Uint16Array(indicesOut), new Float32Array(positionsOut), new Float32Array(normalsOut), new Float32Array(texCoordsOut));
            return mesh;
        }
        static calcNormals(vertices, indices) {
            for (let i = 0; i < indices.length; i += 3) {
                const i0 = indices[i].vertexIndex;
                const i1 = indices[i + 1].vertexIndex;
                const i2 = indices[i + 2].vertexIndex;
                const v1 = vertices[i1].getPos().subVec(vertices[i0].getPos());
                const v2 = vertices[i2].getPos().subVec(vertices[i0].getPos());
                const normal = v1.cross(v2).normalized();
                vertices[i0].setNormal(vertices[i0].getNormal().addVec(normal));
                vertices[i1].setNormal(vertices[i1].getNormal().addVec(normal));
                vertices[i2].setNormal(vertices[i2].getNormal().addVec(normal));
            }
            for (let i = 0; i < vertices.length; i++)
                vertices[i].setNormal(vertices[i].getNormal().normalized());
        }
    }
    ResourceLoader.URL = "res/";
    ResourceLoader.modelsData = new Map();
    ResourceLoader.shadersData = new Map();
    ResourceLoader.texturesData = new Map();
    ResourceLoader.OBJProperties = {
        VERTEX: "v",
        UV: "vt",
        NORMAL: "vn",
        FACE: "f",
        SMOOTH: "s",
        GROUP: "g",
        MAT: "usemtl"
    };
    exports.default = ResourceLoader;
    class OBJIndex {
        constructor() {
            this.hasTexCoords = false;
            this.hasNormals = false;
        }
        static parseIndex(index) {
            let result = new OBJIndex();
            const values = index.split("/");
            result.vertexIndex = parseInt(values[0]) - 1;
            if (values.length > 1) {
                result.texCoordIndex = parseInt(values[1]) - 1;
                result.hasTexCoords = true;
                if (values.length > 2) {
                    result.normalIndex = parseInt(values[2]) - 1;
                    result.hasNormals = true;
                }
            }
            return result;
        }
    }
    class Vertex {
        constructor(pos = new Vector3f_3.default(), texCoord = new Vector2f_1.default(), normal = new Vector3f_3.default()) {
            this.pos = pos;
            this.texCoord = texCoord;
            this.normal = normal;
        }
        getPos() {
            return this.pos;
        }
        setPos(pos) {
            this.pos = pos;
        }
        getTexCoord() {
            return this.texCoord;
        }
        setTexCoord(texCoord) {
            this.texCoord = texCoord;
        }
        getNormal() {
            return this.normal;
        }
        setNormal(normal) {
            this.normal = normal;
        }
    }
    Vertex.SIZE = 8;
});
define("renderEngine/Texture", ["require", "exports", "utils/RenderUtil", "loaders/ResourceLoader"], function (require, exports, RenderUtil_5, ResourceLoader_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Texture {
        constructor(imageURL) {
            this.isReady = false;
            this.image = new Image();
            const instance = this;
            this.image.onload = function () {
                const gl = RenderUtil_5.default.gl;
                instance.texture = gl.createTexture();
                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D, instance.texture);
                gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
                gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, instance.image);
                instance.isReady = true;
            };
            this.image.src = ResourceLoader_3.default.URL + imageURL;
        }
        bind() {
            if (!this.isReady)
                return;
            const gl = RenderUtil_5.default.gl;
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, this.texture);
        }
    }
    exports.default = Texture;
});
define("renderEngine/Material", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Material {
        constructor(texture, color) {
            this.texture = texture;
            this.color = color;
        }
        getTexture() {
            return this.texture;
        }
        setTexture(texture) {
            this.texture = texture;
        }
        getColor() {
            return this.color;
        }
        setColor(color) {
            this.color = color;
        }
    }
    exports.default = Material;
});
define("renderEngine/Transform", ["require", "exports", "renderEngine/Vector3f", "renderEngine/Matrix4f"], function (require, exports, Vector3f_4, Matrix4f_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Transform {
        constructor() {
            this.translation = new Vector3f_4.default(0, 0, 0);
            this.rotation = new Vector3f_4.default(0, 0, 0);
            this.scale = new Vector3f_4.default(1, 1, 1);
        }
        getTransformation() {
            const translationMatrix = new Matrix4f_1.default().translate(this.translation.getX(), this.translation.getY(), this.translation.getZ());
            const rotationMatrix = new Matrix4f_1.default();
            rotationMatrix.rotate(this.rotation.getX(), 1, 0, 0);
            rotationMatrix.rotate(this.rotation.getY(), 0, 1, 0);
            rotationMatrix.rotate(this.rotation.getZ(), 0, 0, 1);
            const scaleMatrix = new Matrix4f_1.default().scale(this.scale.getX(), this.scale.getY(), this.scale.getZ());
            return translationMatrix.multipy(rotationMatrix.multipy(scaleMatrix));
        }
        getTranslation() {
            return this.translation;
        }
        setTranslation(x, y, z) {
            this.translation = new Vector3f_4.default(x, y, z);
        }
        setTranslationVec(translation) {
            this.translation = translation;
        }
        getRotation() {
            return this.rotation;
        }
        setRotationVec(rotation) {
            this.rotation = rotation;
        }
        setRotation(x, y, z) {
            this.rotation = new Vector3f_4.default(x, y, z);
        }
        getScale() {
            return this.scale;
        }
        setScaleVec(scale) {
            this.scale = scale;
        }
        setScale(x, y, z) {
            this.scale = new Vector3f_4.default(x, y, z);
        }
    }
    exports.default = Transform;
});
define("renderEngine/Time", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Time {
        static getTime() {
            return Date.now();
        }
        static getDelta() {
            return Time.delta;
        }
        static setDelta(delta) {
            Time.delta = delta;
        }
    }
    Time.SECOND = 1000;
    Time.delta = 0;
    exports.default = Time;
});
define("renderEngine/Display", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Display {
        static setTitle(title) {
            Display.title = title;
            document.title = Display.title;
        }
        static setDisplay(width, height) {
            Display.width = width;
            Display.height = height;
        }
        static create() {
            let c = document.createElement("canvas");
            c.width = Display.width;
            c.height = Display.height;
            c.style.width = Display.width + "px";
            c.style.height = Display.height + "px";
            document.body.appendChild(c);
            Display.canvas = c;
        }
        static update() {
        }
        static isCloseRequested() {
            return false;
        }
        static getWidth() {
            return Display.width;
        }
        static getHeight() {
            return Display.height;
        }
        static getTitle() {
            return Display.title;
        }
    }
    exports.default = Display;
});
define("input/Keyboard", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Keyboard {
        static create() {
            window.onkeyup = function (e) {
                Keyboard.keys[e.keyCode] = false;
            };
            window.onkeydown = function (e) {
                Keyboard.keys[e.keyCode] = true;
            };
        }
        static isKeyDown(keyCode) {
            return Keyboard.keys[keyCode] === true;
        }
    }
    Keyboard.LEFT = 37;
    Keyboard.UP = 38;
    Keyboard.RIGHT = 39;
    Keyboard.DOWN = 40;
    Keyboard.W = 87;
    Keyboard.A = 65;
    Keyboard.S = 83;
    Keyboard.D = 68;
    Keyboard.SPACE = 32;
    Keyboard.ESCAPE = 27;
    Keyboard.NUM_KEYCODES = 9;
    Keyboard.keys = [];
    exports.default = Keyboard;
});
define("input/Mouse", ["require", "exports", "renderEngine/Display", "renderEngine/Vector2f"], function (require, exports, Display_1, Vector2f_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Mouse {
        static create() {
            Display_1.default.canvas.onmousedown = function (e) {
                Mouse.buttons[e.button] = true;
            };
            Display_1.default.canvas.onmouseup = function (e) {
                Mouse.buttons[e.button] = false;
            };
        }
        static moveCallback(e) {
            let x = Mouse.currentPosition.getX();
            let y = Mouse.currentPosition.getY();
            let x1 = -e.movementX;
            let y1 = -e.movementY;
            if (x === x1 && y === y1) {
                Mouse.currentPosition.setX(0);
                Mouse.currentPosition.setY(0);
            }
            else {
                Mouse.currentPosition.setX(x1);
                Mouse.currentPosition.setY(y1);
            }
        }
        static isButtonDown(buttonCode) {
            return Mouse.buttons[buttonCode] === true;
        }
        static getX() {
            return Mouse.currentPosition.getX();
        }
        static getY() {
            return Mouse.currentPosition.getY();
        }
        static setMouseLock(lock) {
            Mouse.isLocked = lock;
            if (!lock) {
                Display_1.default.canvas.removeEventListener("mousemove", Mouse.moveCallback);
            }
            else {
                Display_1.default.canvas.addEventListener("mousemove", Mouse.moveCallback);
            }
        }
    }
    Mouse.FIRE = 0;
    Mouse.MIDDLE = 1;
    Mouse.RIGHT = 2;
    Mouse.NUM_MOUSE_BUTTONS = 3;
    Mouse.currentPosition = new Vector2f_2.default();
    Mouse.previousPosition = new Vector2f_2.default();
    Mouse.buttons = [];
    Mouse.sensitivity = 0.3;
    Mouse.isLocked = false;
    exports.default = Mouse;
});
define("input/Input", ["require", "exports", "renderEngine/Vector2f", "input/Keyboard", "input/Mouse"], function (require, exports, Vector2f_3, Keyboard_1, Mouse_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Input {
        static update() {
            for (let i = 0; i < Keyboard_1.default.NUM_KEYCODES; i++) {
                Input.lastKeys[i] = Input.getKey(i);
            }
            for (let i = 0; i < Mouse_1.default.NUM_MOUSE_BUTTONS; i++) {
                Input.lastMouse[i] = Input.getMouse(i);
            }
        }
        static getKey(keyCode) {
            return Keyboard_1.default.isKeyDown(keyCode);
        }
        static getKeyDown(keyCode) {
            return Input.getKey(keyCode) && !Input.lastKeys[keyCode];
        }
        static getKeyUp(keyCode) {
            return !Input.getKey(keyCode) && Input.lastKeys[keyCode];
        }
        static getMouse(mouseButton) {
            return Mouse_1.default.isButtonDown(mouseButton);
        }
        static getMouseDown(mouseButton) {
            return Input.getMouse(mouseButton) && !Input.lastMouse[mouseButton];
        }
        static getMouseUp(mouseButton) {
            return !Input.getMouse(mouseButton) && Input.lastMouse[mouseButton];
        }
        static getMouseLock() {
            return Mouse_1.default.isLocked;
        }
        static getMousePosition() {
            let res;
            Input.previousDeltaPos = Input.currentDeltaPos;
            Input.currentDeltaPos = new Vector2f_3.default(Mouse_1.default.getX(), Mouse_1.default.getY());
            if (Input.previousDeltaPos.equals(Input.currentDeltaPos)) {
                res = new Vector2f_3.default(0, 0);
            }
            else {
                res = Input.currentDeltaPos;
            }
            return res;
        }
    }
    Input.lastKeys = new Array(Keyboard_1.default.NUM_KEYCODES);
    Input.lastMouse = new Array(Mouse_1.default.NUM_MOUSE_BUTTONS);
    Input.currentDeltaPos = new Vector2f_3.default();
    Input.previousDeltaPos = new Vector2f_3.default();
    exports.default = Input;
});
define("renderEngine/GameWindow", ["require", "exports", "renderEngine/Display", "input/Keyboard", "input/Mouse"], function (require, exports, Display_2, Keyboard_2, Mouse_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class GameWindow {
        static createWindow(width, height, title) {
            Display_2.default.setTitle(title);
            Display_2.default.setDisplay(width, height);
            Display_2.default.create();
            Keyboard_2.default.create();
            Mouse_2.default.create();
        }
        static render() {
            Display_2.default.update();
        }
        static isCloseRequested() {
            return Display_2.default.isCloseRequested();
        }
        static getWidth() {
            return Display_2.default.getWidth();
        }
        static getHeight() {
            return Display_2.default.getHeight();
        }
        static getTitle() {
            return Display_2.default.getTitle();
        }
        static dispose() {
            console.log("destroy the display");
        }
    }
    exports.default = GameWindow;
});
define("renderEngine/Camera3D", ["require", "exports", "renderEngine/Vector3f", "renderEngine/Matrix4f", "renderEngine/Time", "renderEngine/Display", "input/Input", "input/Keyboard", "input/Mouse"], function (require, exports, Vector3f_5, Matrix4f_2, Time_1, Display_3, Input_1, Keyboard_3, Mouse_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Camera3D {
        constructor(pos = new Vector3f_5.default(0, 0, 0), forward = new Vector3f_5.default(0, 0, 1), up = new Vector3f_5.default(0, 1, 0)) {
            this.yAxis = new Vector3f_5.default(0, 1, 0);
            this.pos = pos;
            this.forward = forward.normalized();
            this.up = up.normalized();
        }
        move(dir, amt) {
            this.pos = this.pos.addVec(dir.mulNum(amt));
        }
        rotateX(angle) {
            let HAxis = this.yAxis.cross(this.forward).normalized();
            this.forward = this.forward.rotate(angle, HAxis).normalized();
            this.up = this.forward.cross(HAxis).normalized();
        }
        rotateY(angle) {
            let HAxis = this.yAxis.cross(this.forward).normalized();
            this.forward = this.forward.rotate(angle, this.yAxis).normalized();
            this.up = this.forward.cross(HAxis).normalized();
        }
        getLeft() {
            return this.forward.cross(this.up).normalized();
        }
        getRight() {
            return this.up.cross(this.forward).normalized();
        }
        input() {
            let moveAmt = 10 * Time_1.default.getDelta();
            let rotationAmt = 100 * Time_1.default.getDelta();
            if (Input_1.default.getKey(Keyboard_3.default.W)) {
                this.move(this.getForward(), -moveAmt);
            }
            if (Input_1.default.getKey(Keyboard_3.default.S)) {
                this.move(this.getForward(), moveAmt);
            }
            if (Input_1.default.getKey(Keyboard_3.default.A)) {
                this.move(this.getLeft(), moveAmt);
            }
            if (Input_1.default.getKey(Keyboard_3.default.D)) {
                this.move(this.getRight(), moveAmt);
            }
            if (Input_1.default.getMouseLock()) {
                const deltaPos = Input_1.default.getMousePosition();
                const rotY = deltaPos.getX() != 0;
                const rotX = deltaPos.getY() != 0;
                if (rotY) {
                    this.rotateY(deltaPos.getX() * Mouse_3.default.sensitivity);
                }
                if (rotX) {
                    this.rotateX(deltaPos.getY() * Mouse_3.default.sensitivity);
                }
            }
            if (Input_1.default.getKey(Keyboard_3.default.UP)) {
                this.rotateX(rotationAmt);
            }
            if (Input_1.default.getKey(Keyboard_3.default.DOWN)) {
                this.rotateX(-rotationAmt);
            }
            if (Input_1.default.getKey(Keyboard_3.default.LEFT)) {
                this.rotateY(rotationAmt);
            }
            if (Input_1.default.getKey(Keyboard_3.default.RIGHT)) {
                this.rotateY(-rotationAmt);
            }
        }
        getProjection() {
            return new Matrix4f_2.default().setPerspective(70, Display_3.default.getWidth() / Display_3.default.getHeight(), 0.1, 1000);
        }
        getViewMatrix() {
            const r = new Matrix4f_2.default().initCamera(this.forward, this.up);
            const t = new Matrix4f_2.default().initTranslate(this.pos.getX(), this.pos.getY(), this.pos.getZ()).invert();
            return r.multipy(t);
        }
        getPos() {
            return this.pos;
        }
        setPos(pos) {
            this.pos = pos;
        }
        getForward() {
            return this.forward;
        }
        setForward(forward) {
            this.forward = forward;
        }
        getUp() {
            return this.up;
        }
        setUp(up) {
            this.up = up;
        }
    }
    exports.default = Camera3D;
});
define("renderEngine/GameObject", ["require", "exports", "renderEngine/Transform"], function (require, exports, Transform_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class GameObject {
        constructor(mesh, shader, material) {
            this.mesh = mesh;
            this.shader = shader;
            this.material = material;
            this.transform = new Transform_1.default();
        }
        update(tick) {
            const sin = Math.sin(tick);
            let r = sin * 180;
            this.transform.setTranslation(0, 0, 0);
            this.transform.setRotation(0, r, 0);
            this.transform.setScale(1, 1, 1);
        }
        render(camera, light) {
            const modelMatrix = this.transform.getTransformation();
            const viewMatrix = camera.getViewMatrix();
            const projectionMatrix = camera.getProjection();
            const modelToView = viewMatrix.multipy(modelMatrix);
            const mvp = projectionMatrix.multipy(modelToView);
            this.shader.setDirectionalLight(light);
            this.shader.update(this.transform.getTransformation(), mvp, this.material);
            this.mesh.draw();
        }
        getTransform() {
            return this.transform;
        }
        getShader() {
            return this.shader;
        }
    }
    exports.default = GameObject;
});
define("engineTester/MainComponent", ["require", "exports", "renderEngine/GameWindow", "utils/RenderUtil", "renderEngine/Display", "utils/MathUtil", "renderEngine/Time", "engineTester/Game", "loaders/ResourceLoader", "input/Mouse", "input/Input", "input/Keyboard"], function (require, exports, GameWindow_1, RenderUtil_6, Display_4, MathUtil_5, Time_2, Game_1, ResourceLoader_4, Mouse_4, Input_2, Keyboard_4) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class MainComponent {
        constructor(callback) {
            this.isRunning = false;
            this.callback = callback;
            this.fpsUI = MathUtil_5.default.createFPSCounterElement();
            GameWindow_1.default.createWindow(MainComponent.WIDTH, MainComponent.HEIGHT, MainComponent.TITLE);
            RenderUtil_6.default.initGraphics(Display_4.default.canvas);
            this.isRunning = false;
            const instance = this;
            document.addEventListener("pointerlockchange", function (e) {
                let lock = document.pointerLockElement === Display_4.default.canvas;
                if (!lock) {
                    instance.stop();
                }
            }, false);
            MathUtil_5.default.loadJSONFile(ResourceLoader_4.default.URL + "resources.json")
                .then((result) => {
                return ResourceLoader_4.default.loadResources(result);
            })
                .then(loaded => {
                this.game = new Game_1.default();
            });
        }
        start() {
            if (this.isRunning) {
                return;
            }
            ;
            this.run();
            this.isRunning = true;
            Mouse_4.default.setMouseLock(true);
            Display_4.default.canvas.requestPointerLock();
        }
        run() {
            let instance = this;
            const frameTime = 1.0 / MainComponent.FRAME_CAP;
            let lastTime = Time_2.default.getTime();
            let unprocessedTime = 0;
            let frames = 0;
            let frameCounter = 0;
            let loop = function (e) {
                let render = false;
                let startTime = Time_2.default.getTime();
                let passedTime = startTime - lastTime;
                lastTime = startTime;
                unprocessedTime += (passedTime / Time_2.default.SECOND);
                frameCounter += passedTime;
                while (unprocessedTime > frameTime) {
                    render = true;
                    unprocessedTime -= frameTime;
                    if (!instance.isRunning) {
                        window.clearInterval(interID);
                    }
                    Time_2.default.setDelta(frameTime);
                    instance.game.input();
                    instance.game.update();
                    if (Input_2.default.getKey(Keyboard_4.default.ESCAPE)) {
                        instance.stop();
                    }
                    if (frameCounter >= Time_2.default.SECOND) {
                        instance.fpsUI.innerHTML = "FPS: " + frames;
                        frames = 0;
                        frameCounter = 0;
                    }
                }
                if (render) {
                    instance.render();
                    frames++;
                }
            };
            let interID = window.setInterval(loop, 1);
        }
        stop() {
            if (!this.isRunning) {
                return;
            }
            this.isRunning = false;
            Mouse_4.default.setMouseLock(false);
            if (this.callback) {
                this.callback();
            }
        }
        render() {
            RenderUtil_6.default.clearScreen();
            this.game.render();
            GameWindow_1.default.render();
        }
        cleanUp() {
            GameWindow_1.default.dispose();
        }
        onMouseLockChange(e) {
        }
    }
    MainComponent.WIDTH = 800;
    MainComponent.HEIGHT = 600;
    MainComponent.TITLE = "Gizmo3D Game Engine";
    MainComponent.FRAME_CAP = 60;
    exports.default = MainComponent;
});
define("engineTester/Game", ["require", "exports", "renderEngine/Vector3f", "renderEngine/Time", "utils/RenderUtil", "renderEngine/DirectionalLight", "renderEngine/Camera3D", "loaders/ResourceLoader", "shaders/Shader", "renderEngine/BaseLight", "input/Input"], function (require, exports, Vector3f_6, Time_3, RenderUtil_7, DirectionalLight_2, Camera3D_1, ResourceLoader_5, Shader_4, BaseLight_2, Input_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Game {
        constructor() {
            this.ready = false;
            this.temp = 0.0;
            this.gameObject = ResourceLoader_5.default.createGameObject({
                mesh: "cube.obj",
                shader: {
                    type: Shader_4.ShaderType.PHONG
                },
                material: {
                    texture: "cube.png",
                    color: new Vector3f_6.default(1, 1, 1)
                }
            }, true);
            this.camera = new Camera3D_1.default();
            this.camera.setPos(new Vector3f_6.default(0, 0, 2));
            this.directionalLight = new DirectionalLight_2.default(new BaseLight_2.default(new Vector3f_6.default(1, 1, 1), 1), new Vector3f_6.default(1, 1, 1));
            const shader = this.gameObject.getShader();
            shader.setAmbientLight(new Vector3f_6.default(0.1, 0.1, 0.1));
            shader.setDirectionalLight(this.directionalLight);
            this.ready = true;
        }
        input() {
            if (!this.ready)
                return;
            this.camera.input();
        }
        update() {
            if (!this.ready)
                return;
            this.temp += Time_3.default.getDelta();
            Input_3.default.update();
            this.gameObject.update(this.temp);
        }
        render() {
            if (!this.ready)
                return;
            RenderUtil_7.default.setClearColor(new Vector3f_6.default(0.2, 0.2, 0.2));
            this.gameObject.render(this.camera, this.directionalLight);
        }
    }
    exports.default = Game;
});
//# sourceMappingURL=main.js.map
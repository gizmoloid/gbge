import Vector3f from "../renderEngine/Vector3f";
import Vector2f from "../renderEngine/Vector2f";
import MathUtil from "../utils/MathUtil";
import Mesh from "../renderEngine/Mesh";
import GameObject from "../renderEngine/GameObject";
import Material from "../renderEngine/Material";
import Texture from "../renderEngine/Texture";
import BasicShader from "../shaders/BasicShader";
import Shader, { ShaderType } from "../shaders/Shader";
import PhongShader from "../shaders/PhongShader";

/**
 * ResourceLoader class
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */

enum Formats {
    OBJ = "obj",
    FBX = "fbx",
    GLSL = "glsl"
}

export default class ResourceLoader {

    public static URL: string = "res/";
    private static modelsData: Map<string, string> = new Map<string, string>();
    private static shadersData: Map<string, string> = new Map<string, string>();
    private static texturesData: Map<string, string> = new Map<string, string>();

    private static OBJProperties = {
        VERTEX: "v",
        UV: "vt",
        NORMAL: "vn",
        FACE: "f",
        SMOOTH: "s",
        GROUP: "g",
        MAT: "usemtl"
    }

    /**
     * 
     * This function initializes all resources from a given .json file
     * Starts by storing the files urls into their respective arrays
     *      models : array with the models urls from json file
     *      shaders: array with the shaders urls from json file
     * 
     * Then loads each file (from respective array) and maps the contents into their respective maps
     *      modelsData: Map with models data
     *      shadersData: Map with shaders data
     * 
     * To get an .obj file, use ResourceseLoader.loadMesh("name-of-the-file")
     * To get an .glsl file, use ResourceseLoader.loadShader("name-of-the-file")
     * 
     * @param json File with the content to be loaded
     * @returns Promisse boolean, true if success, false if fail
     */
    public static async loadResources(json: any): Promise<boolean> {

        let models: Array<string> = Array<string>();
        let shaders: Array<string> = Array<string>();
        let textures: Array<string> = Array<string>();

        //build models urls
        for (let i: number = 0; i < json.models.files.length; i++) {
            models.push(json.models.path + json.models.files[i]);
        }
        //build shaders urls
        for (let i: number = 0; i < json.shaders.files.length; i++) {
            shaders.push(json.shaders.path + json.shaders.files[i]);
        }
        //build textures urls
        for (let i: number = 0; i < json.textures.files.length; i++) {
            textures.push(json.textures.path + json.textures.files[i]);
        }

        return Promise.all(models.map(url =>
            fetch(ResourceLoader.URL + url).then(resp => resp.text())
        )).then((mData) => {

            let key: string;
            for (let i: number = 0; i < mData.length; i++) {
                key = models[i].split("/")[1];
                ResourceLoader.modelsData.set(key, mData[i]);
            }

            return Promise.all(shaders.map(url =>
                fetch(ResourceLoader.URL + url).then(resp => resp.text())
            )).then(sData => {

                for (let i: number = 0; i < sData.length; i++) {
                    key = shaders[i].split("/")[1];
                    ResourceLoader.shadersData.set(key, sData[i]);
                }

                return Promise.all(textures.map(url =>

                    fetch(ResourceLoader.URL + url).then(resp => resp.blob())
                )).then(tData => {

                    for (let i: number = 0; i < tData.length; i++) {

                        key = textures[i].split("/")[1];

                        ResourceLoader.texturesData.set(key, textures[i]);
                    }
                    return true;
                });
            });
        }).catch((error) => {
            return false;
        });
    }

    /**
     * This method gets a shader from the shadersData map by it name.
     * @param shaderName The file name of the shader to be loaded (must include extension .glsl)
     * @returns a string of the shader code
     */
    public static loadShader(shaderName: string): string {

        const sData: string | undefined = ResourceLoader.shadersData.get(shaderName);

        if (!sData) throw new Error("No shader found: " + shaderName);

        return sData;
    }

    /**
     * This method gets a model from the modelsData map by its name. It checks for its
     * extension (.obj or .fbx) and parses the correspondent file.
     * @param meshName The file name of the model to be loaded (must include extension .obj or .fbx)
     * @returns a mesh object
     */
    public static loadMesh(meshName: string, calcNormals: boolean): Mesh {

        const arr: Array<string> = meshName.split(".");
        const fileFormat: string = arr[arr.length - 1];

        const mData: string | undefined = ResourceLoader.modelsData.get(meshName);

        if (!mData) throw new Error("No mesh found: " + meshName);

        switch (fileFormat) {
            case Formats.OBJ:
                return ResourceLoader.parseOBJ(mData, calcNormals);
            case Formats.FBX:
                return ResourceLoader.parseFBX(mData);
            default:
                throw new Error("File format not available: " + meshName);
        }
    }
    public static createGameObject(data: any, calcNormals: boolean): GameObject {

        const mesh: Mesh = ResourceLoader.loadMesh(data.mesh, calcNormals);

        let shader: Shader;

        switch (data.shader.type) {
            case ShaderType.BASIC:
                shader = new BasicShader();
                break;
            case ShaderType.PHONG:
                shader = new PhongShader();
                break;
            case ShaderType.CUSTOM:
                shader = new Shader();
                shader.setShaders(
                    ResourceLoader.loadShader(data.shader.vShader),
                    ResourceLoader.loadShader(data.shader.fShader)
                );
                // shader.setProgram();
                break;
            default:
                shader = new Shader();
                shader.setShaders(
                    ResourceLoader.loadShader("basicVertex.glsl"),
                    ResourceLoader.loadShader("basicFragment.glsl")
                );
            // shader.setProgram();
        }

        mesh.setShader(shader.getProgram());

        const material = new Material(
            new Texture("textures/" + data.material.texture),
            data.material.color
        );
        // const texture : Texture = new Texture("textures/" + data.texture);

        // return new GameObject(mesh, shader, texture);
        return new GameObject(mesh, shader, material);
    }

    /**
     * This method gets a texture from the texturesData map by its name.
     * @param textureName The file name of the texture to be loaded (must include extension .jpg or .png)
     * @returns a Texture object
     */
    public static loadTexture(textureName: string, callback: Function): Texture {

        const arr: Array<string> = textureName.split(".");
        const fileFormat: string = arr[arr.length - 1];

        let counter: number = 0;
        let texture: any;
        const totalTextures = ResourceLoader.texturesData.size;

        let textureURL: string | undefined;
        for (let i: number = 0; i < ResourceLoader.texturesData.size; i++) {

            textureURL = ResourceLoader.texturesData.get(textureName);
            if (textureURL) return new Texture(textureURL);
        }

        return texture;
    }

    /**
     * !NOT WORKING YET!
     * This method parses an FBX file to a Mesh object. Sets the vertices, uvs, normals,
     * animations and indices of the FBX file.
     * More info: https://banexdevblog.wordpress.com/2014/06/23/a-quick-tutorial-about-the-fbx-ascii-format/
     * @param objData The data from the FBX file to be parsed
     * @returns a Mesh object
     */
    private static parseFBX(fbxData: string): Mesh {

        let mesh: Mesh = new Mesh();
        return mesh;
    }

    /**
     * This method parses an OBJ file to a Mesh object. Sets the vertices, uvs, normals and indices 
     * of the OBJ file.
     * More info: http://www.martinreddy.net/gfx/3d/OBJ.spec
     * @param objData The data from the OBJ file to be parsed
     * @returns a Mesh object
     */
    private static parseOBJ(objData: string, calcNormals: boolean): Mesh {

        let mesh: Mesh = new Mesh();
        let name: string = "no name";
        let vertices: Array<Vector3f> = new Array<Vector3f>();
        let texCoords: Array<Vector2f> = new Array<Vector2f>();
        let normals: Array<Vector3f> = new Array<Vector3f>();
        let indices: Array<OBJIndex> = new Array<OBJIndex>();

        let linesArray: Array<string> = objData.split("\n");
        let tokens: Array<string>;
        for (let i = 0; i < linesArray.length; i++) {

            tokens = linesArray[i].split(" ");
            tokens = MathUtil.removeEmptyStrings(tokens);

            switch (tokens[0]) {

                case ResourceLoader.OBJProperties.GROUP:
                    name = tokens[1];
                    break;

                case ResourceLoader.OBJProperties.VERTEX:

                    vertices.push(new Vector3f(
                        parseFloat(tokens[1]),
                        parseFloat(tokens[2]),
                        parseFloat(tokens[3])
                    ));
                    break;

                case ResourceLoader.OBJProperties.UV:
                    texCoords.push(new Vector2f(
                        parseFloat(tokens[1]),
                        parseFloat(tokens[2])
                    ));
                    break;

                case ResourceLoader.OBJProperties.NORMAL:

                    normals.push(new Vector3f(
                        parseFloat(tokens[1]),
                        parseFloat(tokens[2]),
                        parseFloat(tokens[3])
                    ));
                    break;

                case ResourceLoader.OBJProperties.FACE:

                    indices.push(OBJIndex.parseIndex(tokens[1]));
                    indices.push(OBJIndex.parseIndex(tokens[2]));
                    indices.push(OBJIndex.parseIndex(tokens[3]));

                    break;
                default:
                    continue;
            }
        }

        

        let positionsOut: Array<number> = new Array<number>();
        let texCoordsOut: Array<number> = new Array<number>();
        let normalsOut: Array<number> = new Array<number>();
        let indicesOut: Array<number> = new Array<number>();
        for (let i: number = 0; i < indices.length; i++) {

            let currentIndex: OBJIndex = indices[i];
            let currentPosition: Vector3f = vertices[currentIndex.vertexIndex];

            positionsOut.push(currentPosition.getX());
            positionsOut.push(currentPosition.getY());
            positionsOut.push(currentPosition.getZ());

            let currentTexCoord: Vector2f;
            if (currentIndex.hasTexCoords) {
                currentTexCoord = texCoords[currentIndex.texCoordIndex];
            } else {
                currentTexCoord = new Vector2f(0, 0);;
            }

            texCoordsOut.push(currentTexCoord.getX());
            texCoordsOut.push(currentTexCoord.getY());

            let currentNormal: Vector3f;
            if (currentIndex.hasNormals) {
                currentNormal = normals[currentIndex.normalIndex];
            } else {
                currentNormal = new Vector3f(0, 0, 0);
            }
            currentNormal = currentNormal.normalized();
            normalsOut.push(currentNormal.getX());
            normalsOut.push(currentNormal.getY());
            normalsOut.push(currentNormal.getZ());

            indicesOut.push(i);
        }

        mesh.createMeshVAO(
            name,
            new Uint16Array(indicesOut),
            new Float32Array(positionsOut),
            new Float32Array(normalsOut),
            new Float32Array(texCoordsOut)
        );

        return mesh;
    }

    private static calcNormals(vertices:Array<Vertex>, indices:Array<OBJIndex>):void
	{
		for(let i:number = 0; i < indices.length; i += 3)
		{
			const i0:number = indices[i].vertexIndex;
			const i1:number = indices[i + 1].vertexIndex;
			const i2:number = indices[i + 2].vertexIndex;
			
			const v1:Vector3f = vertices[i1].getPos().subVec(vertices[i0].getPos());
			const v2:Vector3f = vertices[i2].getPos().subVec(vertices[i0].getPos());
			
			const normal:Vector3f = v1.cross(v2).normalized();
			
			vertices[i0].setNormal(vertices[i0].getNormal().addVec(normal));
			vertices[i1].setNormal(vertices[i1].getNormal().addVec(normal));
			vertices[i2].setNormal(vertices[i2].getNormal().addVec(normal));
		}
		
		for(let i:number = 0; i < vertices.length; i++)
			vertices[i].setNormal(vertices[i].getNormal().normalized());
	}
}

class OBJIndex {

    public vertexIndex: number;
    public texCoordIndex: number;
    public normalIndex: number;

    public hasTexCoords: boolean = false;
    public hasNormals: boolean = false;

    constructor() {

    }

    public static parseIndex(index: string): OBJIndex {

        let result = new OBJIndex();
        const values: Array<string> = index.split("/");
        result.vertexIndex = parseInt(values[0]) - 1;

        if (values.length > 1) {
            result.texCoordIndex = parseInt(values[1]) - 1;
            result.hasTexCoords = true;
            if (values.length > 2) {
                result.normalIndex = parseInt(values[2]) - 1;
                result.hasNormals = true;
            }
        }

        return result;
    }
}

class Vertex {


    public static SIZE: number = 8;

    private pos: Vector3f;
    private texCoord: Vector2f;
    private normal: Vector3f;

    constructor(
        pos: Vector3f = new Vector3f(),
        texCoord: Vector2f = new Vector2f(),
        normal: Vector3f = new Vector3f()
    ) {

        this.pos = pos;
        this.texCoord = texCoord;
        this.normal = normal;
    }

    public getPos(): Vector3f {
        return this.pos;
    }

    public setPos(pos: Vector3f): void {
        this.pos = pos;
    }

    public getTexCoord(): Vector2f {
        return this.texCoord;
    }

    public setTexCoord(texCoord: Vector2f): void {
        this.texCoord = texCoord;
    }

    public getNormal(): Vector3f {
        return this.normal;
    }

    public setNormal(normal: Vector3f): void {
        this.normal = normal;
    }
}
import Vector3f from "./Vector3f";
import Texture from "./Texture";

export default class Material {

    private texture : Texture;
    private color : Vector3f;

    constructor(texture:Texture, color:Vector3f) {
        
        this.texture = texture;
        this.color = color;
    }

    //GETTERS / SETTERS
    public getTexture():Texture {
        return this.texture;
    }
    public setTexture(texture:Texture):void {
        this.texture = texture;
    }

    public getColor():Vector3f {
        return this.color;
    }
    public setColor(color:Vector3f):void {
        this.color = color;
    }
}
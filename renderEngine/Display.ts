/**
 * Class for Display
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */
export default class Display {

    private static title: string;
    private static width: number;
    private static height: number;
    public static canvas: HTMLCanvasElement;

    public static setTitle(title:string): void {
        Display.title = title;
        document.title = Display.title;
    }

    public static setDisplay(width: number, height: number): void {
        Display.width = width;
        Display.height = height;
    }

    // public static setCursorLock(lock: boolean): void {

        
    // }

    public static create(): void {

        let c: HTMLCanvasElement = document.createElement("canvas");
        c.width = Display.width;
        c.height = Display.height;
        c.style.width = Display.width + "px";
        c.style.height = Display.height + "px";
        document.body.appendChild(c);

        Display.canvas = c;
    }

    public static update(): void {
        // console.log("render to: " + Display.title);
    }

    public static isCloseRequested(): boolean {
        return false;
    }

    public static getWidth(): number {
        return Display.width;
    }
    
    public static getHeight(): number {
        return Display.height;
    }
    
    public static getTitle(): string {
        return Display.title;
    }
}
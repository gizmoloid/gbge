import Vector3f from "./Vector3f";
import Transform from "./Transform";
import Matrix4f from "./Matrix4f";
import Time from "./Time";
import Vector2f from "./Vector2f";
import GameWindow from "./GameWindow";
import Display from "./Display";
import Input from "../input/Input";
import Keyboard from "../input/Keyboard";
import Mouse from "../input/Mouse";

/**
 * Class for the Mtrix4f
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */
export default class Camera3D {

    private yAxis: Vector3f = new Vector3f(0, 1, 0);

    private pos: Vector3f;
    private forward: Vector3f;
    private up: Vector3f;

    constructor(
        pos: Vector3f = new Vector3f(0,0,0),
        forward: Vector3f = new Vector3f(0,0,1),
        up: Vector3f = new Vector3f(0,1,0)) {

        this.pos = pos;
        this.forward = forward.normalized();
        this.up = up.normalized();
    }

    public move(dir: Vector3f, amt: number): void {
       this.pos = this.pos.addVec(dir.mulNum(amt));
    }
    public rotateX(angle: number): void {
        let HAxis: Vector3f = this.yAxis.cross(this.forward).normalized();
        this.forward = this.forward.rotate(angle, HAxis).normalized();
        this.up = this.forward.cross(HAxis).normalized();
    }
    public rotateY(angle: number): void {

        let HAxis: Vector3f = this.yAxis.cross(this.forward).normalized();
        this.forward = this.forward.rotate(angle, this.yAxis).normalized();
        this.up = this.forward.cross(HAxis).normalized();
    }
    public getLeft(): Vector3f {
        return this.forward.cross(this.up).normalized();
    }
    public getRight(): Vector3f {
        return this.up.cross(this.forward).normalized();
    }
    public input(): void {

        let moveAmt: number = 10 * Time.getDelta();
        let rotationAmt: number = 100 * Time.getDelta();

        if(Input.getKey(Keyboard.W)) {
            this.move(this.getForward(), -moveAmt);
        }
        if(Input.getKey(Keyboard.S)) {
            this.move(this.getForward(), moveAmt);
        }

        if(Input.getKey(Keyboard.A)) {
            this.move(this.getLeft(), moveAmt);
        }
        if(Input.getKey(Keyboard.D)) {
            this.move(this.getRight(), moveAmt);
        }

        if(Input.getMouseLock()) {

            const deltaPos = Input.getMousePosition();

            const rotY: boolean = deltaPos.getX() != 0;
            const rotX: boolean = deltaPos.getY() != 0;

            if(rotY) {
                this.rotateY(deltaPos.getX() * Mouse.sensitivity);
            }
            if(rotX) {
                this.rotateX(deltaPos.getY() * Mouse.sensitivity);
            }
        }
        
        if(Input.getKey(Keyboard.UP)) {
            this.rotateX(rotationAmt);
        }
        if(Input.getKey(Keyboard.DOWN)) {
            this.rotateX(-rotationAmt);
        }
        if(Input.getKey(Keyboard.LEFT)) {
            this.rotateY(rotationAmt);
        }
        if(Input.getKey(Keyboard.RIGHT)) {
            this.rotateY(-rotationAmt);
        }
    }

    /**
     * GETTERS / SETTERS
     */
    public getProjection():Matrix4f {

        return new Matrix4f().setPerspective(
            70,
            Display.getWidth() / Display.getHeight(),
            0.1,
            1000
        );
    }
    public getViewMatrix():Matrix4f {

        // console.log(this.pos.getZ());
        // console.log(this.pos.negate().getZ());
        // console.log("########");

        const r : Matrix4f = new Matrix4f().initCamera(this.forward, this.up);
        const t : Matrix4f = new Matrix4f().initTranslate(this.pos.getX(), this.pos.getY(), this.pos.getZ()).invert();

        return r.multipy(t);
    }
    public getPos(): Vector3f {
        return this.pos;
    }
    public setPos(pos: Vector3f): void {
        this.pos = pos;
    }
    public getForward(): Vector3f {
        return this.forward;
    }
    public setForward(forward: Vector3f): void {
        this.forward = forward;
    }
    public getUp(): Vector3f {
        return this.up;
    }
    public setUp(up: Vector3f): void {
        this.up = up;
    }
}
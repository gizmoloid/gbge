import Display from "./Display";
import Input from "../input/Input";
import Keyboard from "../input/Keyboard";
import Mouse from "../input/Mouse";

/**
 * Class that manages the game window properties
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */

export default class GameWindow {
    
    /**
     * Creates a Display object.
     * @param width width of the Display
     * @param height height of the Display
     * @param title title of the Display object
     */
    public static createWindow(width: number, height: number, title: string): void {

        Display.setTitle(title);
        Display.setDisplay(width, height);
        Display.create();

        // Input.create();
        Keyboard.create();
        Mouse.create();
    }

    public static render(): void {
        Display.update();
    }

    public static isCloseRequested(): boolean {
        return Display.isCloseRequested();
    }
    
    public static getWidth(): number {
        return Display.getWidth();
    }
    
    public static getHeight(): number {
        return Display.getHeight();
    }
    
    public static getTitle(): string {
        return Display.getTitle();
    }
    
    public static dispose(): void {

        console.log("destroy the display");
        // Display = null;
    }
}
import RenderUtil from "../utils/RenderUtil";
import ResourceLoader from "../loaders/ResourceLoader";

export default class Texture {

    private image : HTMLImageElement;
    private texture : WebGLTexture | null;

    public isReady : boolean = false;

    constructor(imageURL : string) {

        this.image = new Image();
        const instance = this;
        this.image.onload = function() {

            const gl = RenderUtil.gl;
            instance.texture = gl.createTexture();
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, instance.texture);
            gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            // -- Allocate storage for the texture
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, instance.image);

            instance.isReady = true;
        }
        this.image.src = ResourceLoader.URL + imageURL;
    }

    public bind() : void {

        if(!this.isReady) return;
        
        const gl = RenderUtil.gl;
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
    }
}
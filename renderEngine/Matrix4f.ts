import Vector3f from "./Vector3f";

/**
 * Class for the Mtrix4f
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */

export default class Matrix4f {

    public elements : Float32Array;

    /**
     * Constructor of Matrix4f
     * new matrix is initialized with identity matrix.
     */
    constructor() {
        this.elements = new Float32Array([  1, 0, 0, 0,
                                            0, 1, 0, 0,
                                            0, 0, 1, 0,
                                            0, 0, 0, 1 ]);
    }

    /**
     * Set the identity matrix.
     * @return this
     */
    public setIdentity() : Matrix4f {

        let e = this.elements;

        e[0] = 1;   e[4] = 0;   e[8]  = 0;   e[12] = 0;
        e[1] = 0;   e[5] = 1;   e[9]  = 0;   e[13] = 0;
        e[2] = 0;   e[6] = 0;   e[10] = 1;   e[14] = 0;
        e[3] = 0;   e[7] = 0;   e[11] = 0;   e[15] = 1;

        return this;
    }

    public initCamera(forward: Vector3f, up: Vector3f): Matrix4f {

        let f: Vector3f = forward;
        f = f.normalized();

        let r: Vector3f = up;
        r = r.normalized();
        r = r.cross(f);

        let u: Vector3f = f.cross(r);

        let e = this.elements;

        e[0] = r.getX();    e[4] = r.getY();    e[8]  = r.getZ();   e[12] = 0;
        e[1] = u.getX();    e[5] = u.getY();    e[9]  = u.getZ();   e[13] = 0;
        e[2] = f.getX();    e[6] = f.getY();    e[10] = f.getZ();   e[14] = 0;
        e[3] = 0;           e[7] = 0;           e[11] = 0;          e[15] = 1;

        return this;
    }

    /**
     * Copy matrix.
     * @param src source matrix
     * @return this
     */
    public set(src : Matrix4f) : Matrix4f {

        let i : number;
        let s : Float32Array;
        let d : Float32Array;

        s = src.elements;
        d = this.elements;

        if(s === d) {
            return this;
        }

        for(i = 0; i < 16; ++i) {
            d[i] = s[i];    
        }
        return this;
    }

    /**
     * Multiply the matrix from the right.
     * @param other The multiply matrix
     * @return this
     */
    public concat(other : Matrix4f) : Matrix4f {

        let i : number;
        let e : Float32Array = this.elements;
        let a : Float32Array = this.elements;
        let b : Float32Array = other.elements;
        let ai0 : number;
        let ai1 : number
        let ai2 : number;
        let ai3 : number;

        if(e === b) {
            b = new Float32Array(16);
            for(i = 0; i < 16; ++i) {
                b[i] = e[i];
            }
        }

        for (i = 0; i < 4; i++) {
            ai0     = a[i];
            ai1     = a[i+4];
            ai2     = a[i+8];
            ai3     = a[i+12];
            e[i]    = ai0 * b[0]  + ai1 * b[1]  + ai2 * b[2]  + ai3 * b[3];
            e[i+4]  = ai0 * b[4]  + ai1 * b[5]  + ai2 * b[6]  + ai3 * b[7];
            e[i+8]  = ai0 * b[8]  + ai1 * b[9]  + ai2 * b[10] + ai3 * b[11];
            e[i+12] = ai0 * b[12] + ai1 * b[13] + ai2 * b[14] + ai3 * b[15];
        }

        return this;
    }

    public multipy(other : Matrix4f) : Matrix4f {
        return this.concat(other);
    }

    /**
     * Multiply the three-dimensional vector.
     * @param pos  The multiply vector
     * @return The result of multiplication(Float32Array)
     */
    public multiplyVector3f(pos : Vector3f) : Vector3f {

        const e : Float32Array = this.elements;
        const p : Float32Array = pos.elements;

        const v : Vector3f = new Vector3f();
        let result : Float32Array = v.elements;

        result[0] = p[0] * e[0] + p[1] * e[4] + p[2] * e[ 8] + p[3] * e[12];
        result[1] = p[0] * e[1] + p[1] * e[5] + p[2] * e[ 9] + p[3] * e[13];
        result[2] = p[0] * e[2] + p[1] * e[6] + p[2] * e[10] + p[3] * e[14];
        result[3] = p[0] * e[3] + p[1] * e[7] + p[2] * e[11] + p[3] * e[15];

        return v;
    }

    /**
     * Multiply the four-dimensional vector.
     * @param pos  The multiply vector
     * @return The result of multiplication(Float32Array)
     */
    // public multiplyVector4(pos) : Vector4f {
    //     const e : Float32Array = this.elements;
    //     const p : Float32Array = pos.elements;
    //     let v = new Vector4f();
    //     let result = v.elements;
      
    //     result[0] = p[0] * e[0] + p[1] * e[4] + p[2] * e[ 8] + p[3] * e[12];
    //     result[1] = p[0] * e[1] + p[1] * e[5] + p[2] * e[ 9] + p[3] * e[13];
    //     result[2] = p[0] * e[2] + p[1] * e[6] + p[2] * e[10] + p[3] * e[14];
    //     result[3] = p[0] * e[3] + p[1] * e[7] + p[2] * e[11] + p[3] * e[15];
      
    //     return v;
    // };

    /**
     * Transpose the matrix.
     * @return this
     */
    public transpose() : Matrix4f {

        let e : Float32Array;
        let t : number;
      
        e = this.elements;
      
        t = e[ 1];  e[ 1] = e[ 4];  e[ 4] = t;
        t = e[ 2];  e[ 2] = e[ 8];  e[ 8] = t;
        t = e[ 3];  e[ 3] = e[12];  e[12] = t;
        t = e[ 6];  e[ 6] = e[ 9];  e[ 9] = t;
        t = e[ 7];  e[ 7] = e[13];  e[13] = t;
        t = e[11];  e[11] = e[14];  e[14] = t;
      
        return this;
    };

    /**
     * Calculate the inverse matrix of specified matrix, and set to this.
     * @param other The source matrix
     * @return this
     */
    public setInverseOf(other) : Matrix4f {
        let i : number;
        let s : Matrix4f;
        let d : Float32Array;
        let inv : Float32Array;
        let det : number;
      
        s = other.elements;
        d = this.elements;
        inv = new Float32Array(16);
      
        inv[0]  =   s[5]*s[10]*s[15] - s[5] *s[11]*s[14] - s[9] *s[6]*s[15]
                  + s[9]*s[7] *s[14] + s[13]*s[6] *s[11] - s[13]*s[7]*s[10];
        inv[4]  = - s[4]*s[10]*s[15] + s[4] *s[11]*s[14] + s[8] *s[6]*s[15]
                  - s[8]*s[7] *s[14] - s[12]*s[6] *s[11] + s[12]*s[7]*s[10];
        inv[8]  =   s[4]*s[9] *s[15] - s[4] *s[11]*s[13] - s[8] *s[5]*s[15]
                  + s[8]*s[7] *s[13] + s[12]*s[5] *s[11] - s[12]*s[7]*s[9];
        inv[12] = - s[4]*s[9] *s[14] + s[4] *s[10]*s[13] + s[8] *s[5]*s[14]
                  - s[8]*s[6] *s[13] - s[12]*s[5] *s[10] + s[12]*s[6]*s[9];
      
        inv[1]  = - s[1]*s[10]*s[15] + s[1] *s[11]*s[14] + s[9] *s[2]*s[15]
                  - s[9]*s[3] *s[14] - s[13]*s[2] *s[11] + s[13]*s[3]*s[10];
        inv[5]  =   s[0]*s[10]*s[15] - s[0] *s[11]*s[14] - s[8] *s[2]*s[15]
                  + s[8]*s[3] *s[14] + s[12]*s[2] *s[11] - s[12]*s[3]*s[10];
        inv[9]  = - s[0]*s[9] *s[15] + s[0] *s[11]*s[13] + s[8] *s[1]*s[15]
                  - s[8]*s[3] *s[13] - s[12]*s[1] *s[11] + s[12]*s[3]*s[9];
        inv[13] =   s[0]*s[9] *s[14] - s[0] *s[10]*s[13] - s[8] *s[1]*s[14]
                  + s[8]*s[2] *s[13] + s[12]*s[1] *s[10] - s[12]*s[2]*s[9];
      
        inv[2]  =   s[1]*s[6]*s[15] - s[1] *s[7]*s[14] - s[5] *s[2]*s[15]
                  + s[5]*s[3]*s[14] + s[13]*s[2]*s[7]  - s[13]*s[3]*s[6];
        inv[6]  = - s[0]*s[6]*s[15] + s[0] *s[7]*s[14] + s[4] *s[2]*s[15]
                  - s[4]*s[3]*s[14] - s[12]*s[2]*s[7]  + s[12]*s[3]*s[6];
        inv[10] =   s[0]*s[5]*s[15] - s[0] *s[7]*s[13] - s[4] *s[1]*s[15]
                  + s[4]*s[3]*s[13] + s[12]*s[1]*s[7]  - s[12]*s[3]*s[5];
        inv[14] = - s[0]*s[5]*s[14] + s[0] *s[6]*s[13] + s[4] *s[1]*s[14]
                  - s[4]*s[2]*s[13] - s[12]*s[1]*s[6]  + s[12]*s[2]*s[5];
      
        inv[3]  = - s[1]*s[6]*s[11] + s[1]*s[7]*s[10] + s[5]*s[2]*s[11]
                  - s[5]*s[3]*s[10] - s[9]*s[2]*s[7]  + s[9]*s[3]*s[6];
        inv[7]  =   s[0]*s[6]*s[11] - s[0]*s[7]*s[10] - s[4]*s[2]*s[11]
                  + s[4]*s[3]*s[10] + s[8]*s[2]*s[7]  - s[8]*s[3]*s[6];
        inv[11] = - s[0]*s[5]*s[11] + s[0]*s[7]*s[9]  + s[4]*s[1]*s[11]
                  - s[4]*s[3]*s[9]  - s[8]*s[1]*s[7]  + s[8]*s[3]*s[5];
        inv[15] =   s[0]*s[5]*s[10] - s[0]*s[6]*s[9]  - s[4]*s[1]*s[10]
                  + s[4]*s[2]*s[9]  + s[8]*s[1]*s[6]  - s[8]*s[2]*s[5];
      
        det = s[0]*inv[0] + s[1]*inv[4] + s[2]*inv[8] + s[3]*inv[12];
        if (det === 0) {
          return this;
        }
      
        det = 1 / det;
        for (i = 0; i < 16; i++) {
          d[i] = inv[i] * det;
        }
      
        return this;
    };

    /**
     * Calculate the inverse matrix of this, and set to this.
     * @return this
     */
    public invert() : Matrix4f {
        return this.setInverseOf(this);
    };

    /**
     * Set the orthographic projection matrix.
     * @param left The coordinate of the left of clipping plane.
     * @param right The coordinate of the right of clipping plane.
     * @param bottom The coordinate of the bottom of clipping plane.
     * @param top The coordinate of the top top clipping plane.
     * @param near The distances to the nearer depth clipping plane. This value is minus if the plane is to be behind the viewer.
     * @param far The distances to the farther depth clipping plane. This value is minus if the plane is to be behind the viewer.
     * @return this
     */
    public setOrtho(left : number,
                    right : number,
                    bottom : number,
                    top : number, 
                    near : number,
                    far : number) : Matrix4f {

        let e : Float32Array;
        let rw : number;
        let rh : number;
        let rd : number;
      
        if (left === right || bottom === top || near === far) {
          throw "Null Frustum!";
        }
      
        rw = 1 / (right - left);
        rh = 1 / (top - bottom);
        rd = 1 / (far - near);
      
        e = this.elements;
      
        e[0]  = 2 * rw;
        e[1]  = 0;
        e[2]  = 0;
        e[3]  = 0;
      
        e[4]  = 0;
        e[5]  = 2 * rh;
        e[6]  = 0;
        e[7]  = 0;
      
        e[8]  = 0;
        e[9]  = 0;
        e[10] = -2 * rd;
        e[11] = 0;
      
        e[12] = -(right + left) * rw;
        e[13] = -(top + bottom) * rh;
        e[14] = -(far + near) * rd;
        e[15] = 1;
      
        return this;
    };

    /**
     * Multiply the orthographic projection matrix from the right.
     * @param left The coordinate of the left of clipping plane.
     * @param right The coordinate of the right of clipping plane.
     * @param bottom The coordinate of the bottom of clipping plane.
     * @param top The coordinate of the top top clipping plane.
     * @param near The distances to the nearer depth clipping plane. This value is minus if the plane is to be behind the viewer.
     * @param far The distances to the farther depth clipping plane. This value is minus if the plane is to be behind the viewer.
     * @return this
     */
    public ortho(   left : number,
                    right : number,
                    bottom : number,
                    top : number,
                    near : number,
                    far : number) {

        return this.concat(new Matrix4f().setOrtho(left, right, bottom, top, near, far));
    };

    /**
     * Set the perspective projection matrix.
     * @param left The coordinate of the left of clipping plane.
     * @param right The coordinate of the right of clipping plane.
     * @param bottom The coordinate of the bottom of clipping plane.
     * @param top The coordinate of the top top clipping plane.
     * @param near The distances to the nearer depth clipping plane. This value must be plus value.
     * @param far The distances to the farther depth clipping plane. This value must be plus value.
     * @return this
     */
    public setFrustum(left, right, bottom, top, near, far) : Matrix4f {

        let e : Float32Array;
        let rw : number;
        let rh : number;
        let rd : number;
      
        if (left === right || top === bottom || near === far) {
          throw "Null Frustum";
        }
        if (near <= 0) {
          throw "near <= 0";
        }
        if (far <= 0) {
          throw "far <= 0";
        }
      
        rw = 1 / (right - left);
        rh = 1 / (top - bottom);
        rd = 1 / (far - near);
      
        e = this.elements;
      
        e[ 0] = 2 * near * rw;
        e[ 1] = 0;
        e[ 2] = 0;
        e[ 3] = 0;
      
        e[ 4] = 0;
        e[ 5] = 2 * near * rh;
        e[ 6] = 0;
        e[ 7] = 0;
      
        e[ 8] = (right + left) * rw;
        e[ 9] = (top + bottom) * rh;
        e[10] = -(far + near) * rd;
        e[11] = -1;
      
        e[12] = 0;
        e[13] = 0;
        e[14] = -2 * near * far * rd;
        e[15] = 0;
      
        return this;
    };

    /**
     * Multiply the perspective projection matrix from the right.
     * @param left The coordinate of the left of clipping plane.
     * @param right The coordinate of the right of clipping plane.
     * @param bottom The coordinate of the bottom of clipping plane.
     * @param top The coordinate of the top top clipping plane.
     * @param near The distances to the nearer depth clipping plane. This value must be plus value.
     * @param far The distances to the farther depth clipping plane. This value must be plus value.
     * @return this
     */
    public frustum( left : number,
                    right : number,
                    bottom : number,
                    top : number,
                    near : number,
                    far : number) : Matrix4f {

        return this.concat(new Matrix4f().setFrustum(left, right, bottom, top, near, far));
    };
    
    /**
     * Set the perspective projection matrix by fovy and aspect.
     * @param fovy The angle between the upper and lower sides of the frustum.
     * @param aspect The aspect ratio of the frustum. (width/height)
     * @param near The distances to the nearer depth clipping plane. This value must be plus value.
     * @param far The distances to the farther depth clipping plane. This value must be plus value.
     * @return this
     */
    public setPerspective(  fovy : number,
                            aspect : number,
                            near : number,
                            far : number) : Matrix4f {

        let e : Float32Array;
        let rd : number;
        let s : number;
        let ct : number;
      
        if (near === far || aspect === 0) {
          throw "Null Frustum";
        }
        if (near <= 0) {
          throw "Near <= 0";
        }
        if (far <= 0) {
          throw "Far <= 0";
        }
      
        fovy = Math.PI * fovy / 180 / 2;
        s = Math.sin(fovy);
        if (s === 0) {
          throw "Null Frustum";
        }
      
        rd = 1 / (far - near);
        ct = Math.cos(fovy) / s;
      
        e = this.elements;
      
        e[0]  = ct / aspect;
        e[1]  = 0;
        e[2]  = 0;
        e[3]  = 0;
      
        e[4]  = 0;
        e[5]  = ct;
        e[6]  = 0;
        e[7]  = 0;
      
        e[8]  = 0;
        e[9]  = 0;
        e[10] = -(far + near) * rd;
        e[11] = -1;
      
        e[12] = 0;
        e[13] = 0;
        e[14] = -2 * near * far * rd;
        e[15] = 0;
      
        return this;
    };

    /**
     * Multiply the perspective projection matrix from the right.
     * @param fovy The angle between the upper and lower sides of the frustum.
     * @param aspect The aspect ratio of the frustum. (width/height)
     * @param near The distances to the nearer depth clipping plane. This value must be plus value.
     * @param far The distances to the farther depth clipping plane. This value must be plus value.
     * @return this
     */
    public perspective( fovy : number, 
                        aspect : number,
                        near : number,
                        far : number) {
        return this.concat(new Matrix4f().setPerspective(fovy, aspect, near, far));
    };
    
    /**
     * Set the matrix for scaling.
     * @param x The scale factor along the X axis
     * @param y The scale factor along the Y axis
     * @param z The scale factor along the Z axis
     * @return this
     */
    public setScale(x : number, y : number, z : number) : Matrix4f {
        let e : Float32Array = this.elements;
        e[0] = x;  e[4] = 0;  e[8]  = 0;  e[12] = 0;
        e[1] = 0;  e[5] = y;  e[9]  = 0;  e[13] = 0;
        e[2] = 0;  e[6] = 0;  e[10] = z;  e[14] = 0;
        e[3] = 0;  e[7] = 0;  e[11] = 0;  e[15] = 1;
        return this;
    };

    /**
     * Multiply the matrix for scaling from the right.
     * @param x The scale factor along the X axis
     * @param y The scale factor along the Y axis
     * @param z The scale factor along the Z axis
     * @return this
     */
    public scale(x : number, y : number, z : number) : Matrix4f {
        let e : Float32Array = this.elements;
        e[0] *= x;  e[4] *= y;  e[8]  *= z;
        e[1] *= x;  e[5] *= y;  e[9]  *= z;
        e[2] *= x;  e[6] *= y;  e[10] *= z;
        e[3] *= x;  e[7] *= y;  e[11] *= z;
        return this;
    };

    /**
     * Set the matrix for translation.
     * @param x The X value of a translation.
     * @param y The Y value of a translation.
     * @param z The Z value of a translation.
     * @return this
     */
    public initTranslate(x : number, y : number, z : number) : Matrix4f {
        let e : Float32Array = this.elements;
        e[0] = 1;  e[4] = 0;  e[8]  = 0;  e[12] = x;
        e[1] = 0;  e[5] = 1;  e[9]  = 0;  e[13] = y;
        e[2] = 0;  e[6] = 0;  e[10] = 1;  e[14] = z;
        e[3] = 0;  e[7] = 0;  e[11] = 0;  e[15] = 1;
        return this;
    };
    public setTranslateVec(vec: Vector3f) : Matrix4f {
        return this.initTranslate(vec.getX(), vec.getY(), vec.getZ());
    };

    /**
     * Multiply the matrix for translation from the right.
     * @param x The X value of a translation.
     * @param y The Y value of a translation.
     * @param z The Z value of a translation.
     * @return this
     */
    public translate(x : number, y : number, z : number) : Matrix4f {
        let e : Float32Array = this.elements;
        e[12] += e[0] * x + e[4] * y + e[8]  * z;
        e[13] += e[1] * x + e[5] * y + e[9]  * z;
        e[14] += e[2] * x + e[6] * y + e[10] * z;
        e[15] += e[3] * x + e[7] * y + e[11] * z;
        return this;
    };
    public translateVec(pos:Vector3f):Matrix4f {
        return this.translate(pos.getX(),pos.getY(),pos.getZ());
    }
    
    /**
     * Set the matrix for rotation.
     * The vector of rotation axis may not be normalized.
     * @param angle The angle of rotation (degrees)
     * @param x The X coordinate of vector of rotation axis.
     * @param y The Y coordinate of vector of rotation axis.
     * @param z The Z coordinate of vector of rotation axis.
     * @return this
     */
    public setRotate(angle : number, x : number, y : number, z : number) : Matrix4f {
        
        let e : Float32Array = this.elements;
        let s : number;
        let c : number;
        let len : number;
        let rlen : number;
        let nc : number;
        let xy : number;
        let yz : number;
        let zx : number;
        let xs : number;
        let ys : number;
        let zs : number;
      
        angle = Math.PI * angle / 180;
        e = this.elements;
      
        s = Math.sin(angle);
        c = Math.cos(angle);
      
        if (0 !== x && 0 === y && 0 === z) {
          // Rotation around X axis
          if (x < 0) {
            s = -s;
          }
          e[0] = 1;  e[4] = 0;  e[ 8] = 0;  e[12] = 0;
          e[1] = 0;  e[5] = c;  e[ 9] =-s;  e[13] = 0;
          e[2] = 0;  e[6] = s;  e[10] = c;  e[14] = 0;
          e[3] = 0;  e[7] = 0;  e[11] = 0;  e[15] = 1;
        } else if (0 === x && 0 !== y && 0 === z) {
          // Rotation around Y axis
          if (y < 0) {
            s = -s;
          }
          e[0] = c;  e[4] = 0;  e[ 8] = s;  e[12] = 0;
          e[1] = 0;  e[5] = 1;  e[ 9] = 0;  e[13] = 0;
          e[2] =-s;  e[6] = 0;  e[10] = c;  e[14] = 0;
          e[3] = 0;  e[7] = 0;  e[11] = 0;  e[15] = 1;
        } else if (0 === x && 0 === y && 0 !== z) {
          // Rotation around Z axis
          if (z < 0) {
            s = -s;
          }
          e[0] = c;  e[4] =-s;  e[ 8] = 0;  e[12] = 0;
          e[1] = s;  e[5] = c;  e[ 9] = 0;  e[13] = 0;
          e[2] = 0;  e[6] = 0;  e[10] = 1;  e[14] = 0;
          e[3] = 0;  e[7] = 0;  e[11] = 0;  e[15] = 1;
        } else {
          // Rotation around another axis
          len = Math.sqrt(x*x + y*y + z*z);
          if (len !== 1) {
            rlen = 1 / len;
            x *= rlen;
            y *= rlen;
            z *= rlen;
          }
          nc = 1 - c;
          xy = x * y;
          yz = y * z;
          zx = z * x;
          xs = x * s;
          ys = y * s;
          zs = z * s;
      
          e[ 0] = x*x*nc +  c;
          e[ 1] = xy *nc + zs;
          e[ 2] = zx *nc - ys;
          e[ 3] = 0;
      
          e[ 4] = xy *nc - zs;
          e[ 5] = y*y*nc +  c;
          e[ 6] = yz *nc + xs;
          e[ 7] = 0;
      
          e[ 8] = zx *nc + ys;
          e[ 9] = yz *nc - xs;
          e[10] = z*z*nc +  c;
          e[11] = 0;
      
          e[12] = 0;
          e[13] = 0;
          e[14] = 0;
          e[15] = 1;
        }
      
        return this;
    };
    
    /**
     * Multiply the matrix for rotation from the right.
     * The vector of rotation axis may not be normalized.
     * @param angle The angle of rotation (degrees)
     * @param x The X coordinate of vector of rotation axis.
     * @param y The Y coordinate of vector of rotation axis.
     * @param z The Z coordinate of vector of rotation axis.
     * @return this
     */
    public rotate(angle : number, x : number, y : number, z : number) : Matrix4f {
        return this.concat(new Matrix4f().setRotate(angle, x, y, z));
    };
    
    /**
     * Set the viewing matrix.
     * @param eyePos The position of the eye point.
     * @param targetPos The position of the reference point.
     * @param up The direction of the up vector.
     * @return this
     */
    public setLookAt(eyePos : Vector3f, targetPos : Vector3f, up : Vector3f) {

        let e : Float32Array;
        let fx : number;
        let fy : number;
        let fz : number;
        let rlf : number;
        let sx : number;
        let sy : number;
        let sz : number;
        let rls : number;
        let ux : number;
        let uy : number;
        let uz : number;
      
        fx = targetPos.getX() - eyePos.getX();
        fy = targetPos.getY() - eyePos.getY();
        fz = targetPos.getZ() - eyePos.getZ();
      
        // Normalize f.
        rlf = 1 / Math.sqrt(fx*fx + fy*fy + fz*fz);
        fx *= rlf;
        fy *= rlf;
        fz *= rlf;
      
        // Calculate cross product of f and up.
        sx = fy * up.getZ() - fz * up.getY();
        sy = fz * up.getX() - fx * up.getZ();
        sz = fx * up.getY() - fy * up.getX();


      
        // Normalize s.
        rls = 1 / Math.sqrt(sx*sx + sy*sy + sz*sz);



        sx *= rls;
        sy *= rls;
        sz *= rls;
      
        // Calculate cross product of s and f.
        ux = sy * fz - sz * fy;
        uy = sz * fx - sx * fz;
        uz = sx * fy - sy * fx;
      
        // Set to this.
        e = this.elements;
        e[0] = sx;
        e[1] = ux;
        e[2] = -fx;
        e[3] = 0;
      
        e[4] = sy;
        e[5] = uy;
        e[6] = -fy;
        e[7] = 0;
      
        e[8] = sz;
        e[9] = uz;
        e[10] = -fz;
        e[11] = 0;
      
        e[12] = 0;
        e[13] = 0;
        e[14] = 0;
        e[15] = 1;

      
        // Translate.
        return this.translate(-eyePos.getX(), -eyePos.getY(), -eyePos.getZ());
    };

    /**
     * Multiply the viewing matrix from the right.
     * @param eyePos The position of the eye point.
     * @param targetPos The position of the reference point.
     * @param up The direction of the up vector.
     * @return this
     */
    public lookAt(  eyePos : Vector3f, targetPos : Vector3f, up : Vector3f) : Matrix4f {
        return this.concat(new Matrix4f().setLookAt(eyePos, targetPos, up));
    };

    /**
     * Multiply the matrix for project vertex to plane from the right.
     * @param plane The array[A, B, C, D] of the equation of plane "Ax + By + Cz + D = 0".
     * @param light The array which stored coordinates of the light. if light[3]=0, treated as parallel light.
     * @return this
     */
    public dropShadow(plane : Array<number>, light : Array<number>) : Matrix4f {
        let mat : Matrix4f = new Matrix4f();
        let e : Float32Array = mat.elements;
      
        var dot = plane[0] * light[0] + plane[1] * light[1] + plane[2] * light[2] + plane[3] * light[3];
      
        e[ 0] = dot - light[0] * plane[0];
        e[ 1] =     - light[1] * plane[0];
        e[ 2] =     - light[2] * plane[0];
        e[ 3] =     - light[3] * plane[0];
      
        e[ 4] =     - light[0] * plane[1];
        e[ 5] = dot - light[1] * plane[1];
        e[ 6] =     - light[2] * plane[1];
        e[ 7] =     - light[3] * plane[1];
      
        e[ 8] =     - light[0] * plane[2];
        e[ 9] =     - light[1] * plane[2];
        e[10] = dot - light[2] * plane[2];
        e[11] =     - light[3] * plane[2];
      
        e[12] =     - light[0] * plane[3];
        e[13] =     - light[1] * plane[3];
        e[14] =     - light[2] * plane[3];
        e[15] = dot - light[3] * plane[3];
      
        return this.concat(mat);
    }

    /**
     * Multiply the matrix for project vertex to plane from the right.(Projected by parallel light.)
     * @param normX, normY, normZ The normal vector of the plane.(Not necessary to be normalized.)
     * @param planeX, planeY, planeZ The coordinate of arbitrary points on a plane.
     * @param lightX, lightY, lightZ The vector of the direction of light.(Not necessary to be normalized.)
     * @return this
     */   
    public dropShadowDirectionally( normX : number, normY : number, normZ : number,
                                    planeX : number, planeY : number, planeZ : number,
                                    lightX : number, lightY : number, lightZ : number) {
        let a : number = planeX * normX + planeY * normY + planeZ * normZ;
        return this.dropShadow([normX, normY, normZ, -a], [lightX, lightY, lightZ, 0]);
    }
}
  
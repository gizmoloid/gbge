import Vector3f from "./Vector3f";
import MathUtil from "../utils/MathUtil";

export default class Quaternion {

    private x: number;
    private y: number;
    private z: number;
    private w: number; 

    constructor(x: number=0, y: number=0, z: number=0, w: number=0) {

        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public length(): number {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
    }

    public normalize(): Quaternion {

        let length = this.length();

        this.x /= length;
        this.y /= length;
        this.z /= length;
        this.w /= length;

        return this;
    }

    public conjugate(): Quaternion {
        return new Quaternion(-this.x, -this.y, -this.z, this.w);
    }

    public mulQuat(r: Quaternion): Quaternion {

        let _w: number = this.w * r.getW() - this.x * r.getX() - this.y * r.getY() - this.z * r.getZ();
        let _x: number = this.x * r.getW() + this.w * r.getX() + this.y * r.getZ() - this.z * r.getY();
        let _y: number = this.y * r.getW() + this.w * r.getY() + this.z * r.getX() - this.x * r.getZ();
        let _z: number = this.z * r.getW() + this.w * r.getZ() + this.x * r.getY() - this.y * r.getX();

        return new Quaternion(_x, _y, _z, _w);
    }

    public mulVec(r: Vector3f): Quaternion {

        let w_: number = -this.x * r.getX() - this.y * r.getY() - this.z * r.getZ();
        let x_: number =  this.w * r.getX() + this.y * r.getZ() - this.z * r.getY();
        let y_: number =  this.w * r.getY() + this.z * r.getX() - this.x * r.getZ();
        let z_: number =  this.w * r.getZ() + this.x * r.getY() - this.y * r.getX();

        return new Quaternion(x_, y_, z_, w_);
    }

    public rotationAxisToQuaternion(angle : number, axis : Vector3f) : void {

       const angle_1 : number = MathUtil.toRadians(angle);
       const angle_2 : number = angle_1 * 0.5;

       const sine : number = Math.sin(angle_2);

       this.w = Math.cos(angle_2);
       
       this.x = axis.getX() * sine;
       this.y = axis.getY() * sine;
       this.z = axis.getZ() * sine;
    }

    //GETTERS / SETTERS 
    public getX(): number {
        return this.x;
    }
    public getY(): number {
        return this.y;
    }
    public getZ(): number {
        return this.z;
    }
    public getW(): number {
        return this.w;
    }
    public setX(x: number): void {
        this.x = x;
    }
    public setY(y: number): void {
        this.y = y;
    }
    public setZ(z: number): void {
        this.z = z;
    }
    public setW(w: number): void {
        this.w = w;
    }
}
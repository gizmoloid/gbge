import Vector3f from "./Vector3f";

/**
 * Base Light class
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */
export default class BaseLight {

    private color:Vector3f;
    private intensity:number;

    constructor(color:Vector3f, intensity:number) {
        
        this.color = color;
        this.intensity = intensity;
    }

    //GETTERS / SETTERS
    public getColor():Vector3f {
        return this.color;
    }
    public setColor(color:Vector3f):void {
        this.color = color;
    }
    public getIntensity():number {
        return this.intensity;
    }
    public setIntensity(intensity:number):void {
        this.intensity = intensity;
    }
}
import MathUtil from "../utils/MathUtil";

/**
 * Class for the Vector2f
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */

export default class Vector2f extends Object{

    private x: number;
    private y: number;

    public elements : Float32Array;

    constructor(x: number=0, y: number=0) {

        super();

        this.x = x;
        this.y = y;

        this.elements = new Float32Array(2);
        this.elements[0] = this.x;
        this.elements[1] = this.y;
    }

    public length(): number {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }
    public dot(r: Vector2f): number {
        return this.x * r.getX() + this.y * r.getY();
    }
    public normalized(): Vector2f {

        const length: number = this.length();
        return new Vector2f(this.x / length, this.y / length);
    }

    public rotate(angle: number): Vector2f {

        let rad: number = MathUtil.toRadians(angle);
        let cos: number = Math.cos(rad);
        let sin: number = Math.sin(rad);

        return new Vector2f(this.x * cos - this.y * sin, this.x * sin + this.y * cos);
    }

    //OPERATORS
    public addVec(r: Vector2f): Vector2f {
        return new Vector2f(this.x + r.getX(), this.y + r.getY());
    }
    public addNum(r: number): Vector2f {
        return new Vector2f(this.x + r, this.y + r);
    }
    public subVec(r: Vector2f): Vector2f {
        return new Vector2f(this.x - r.getX(), this.y - r.getY());
    }
    public subNum(r: number): Vector2f {
        return new Vector2f(this.x - r, this.y - r);
    }
    public mulVec(r: Vector2f): Vector2f {
        return new Vector2f(this.x * r.getX(), this.y * r.getY());
    }
    public mulNum(r: number): Vector2f {
        return new Vector2f(this.x * r, this.y * r);
    }
    public divVec(r: Vector2f): Vector2f {
        return new Vector2f(this.x / r.getX(), this.y / r.getY());
    }
    public divNum(r: number): Vector2f {
        return new Vector2f(this.x / r, this.y / r);
    }

    public equals(vec: Vector2f): boolean {
        return vec.getX() === this.getX() && vec.getY() === this.getY();
    }
    //GETERS SETTERS
    public getX(): number {
        return this.x;
    }
    public getY(): number {
        return this.y;
    }
    
    public setX(x: number): void {
        this.x = x;
    }
    public setY(y: number): void {
        this.y = y;
    }

    public toString(): string {
        return "(x: " + this.x + " y: " + this.y + ")";
    }
}
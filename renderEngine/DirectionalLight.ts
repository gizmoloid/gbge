import BaseLight from "./BaseLight";
import Vector3f from "./Vector3f";

/**
 * Directional Light class
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */
export default class DirectionalLight {

    private base: BaseLight;
    private direction:Vector3f;

    constructor(base: BaseLight, direction: Vector3f) {

        this.base = base;
        this.direction = direction.normalized();
    }

    //GETTERS / SETTERS
    public getBase():BaseLight {
        return this.base;
    }
    public setBase(base:BaseLight):void {
        this.base = base;
    }
    public getDirection():Vector3f {
        return this.direction;
    }
    public setDirection(direction:Vector3f):void {
        this.direction = direction;
    }
}
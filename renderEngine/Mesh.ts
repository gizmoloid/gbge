import RenderUtil from "../utils/RenderUtil";

/**
 * Class Mesh
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */

export default class Mesh {

    //Vertex array object
    private vao : WebGLVertexArrayObject | null;

    private indexCount : number = 0;

    //Index Buffer Object
    private ibo: WebGLBuffer | null;
    private indices : Uint16Array;
    //Vertex Buffer Object
    private vbo: WebGLBuffer | null;
    private vertices : Float32Array;
    //Normals buffer size
    private nbo: WebGLBuffer | null;
    private normals : Float32Array;
    //UV buffer size
    private UVbo: WebGLBuffer | null;
    private uvs : Float32Array;


    /**
     * Creates a buffer and the default shader program
     */
    constructor() {}

    /**
     * Adds vertices to buffer
     * @param vertices
     *      array of vertices to add to buffer
     * */

    public createMeshVAO(
        name : string,
        indices : Uint16Array,
        verts : Float32Array,
        normals : Float32Array,
        UVs : Float32Array) {

        this.indices = indices;
        this.vertices = verts;
        this.uvs = UVs;
        this.normals = normals;
    }

    public draw(): void {

        const gl: WebGL2RenderingContext = RenderUtil.gl;

        gl.bindVertexArray(this.vao);
        gl.drawElementsInstanced(gl.TRIANGLES, this.indexCount, gl.UNSIGNED_SHORT, 0, 1);
    }

    public setShader(program : WebGLProgram | null) : void {

        const gl:WebGL2RenderingContext = RenderUtil.gl;

        const vertexAttributeLocation : number = gl.getAttribLocation(program, "a_position");
        const texCoordAttributeLocation : number = gl.getAttribLocation(program, "a_texcoord");
        const normalAttributeLocation : number = gl.getAttribLocation(program, "a_normal");


        //Setup Indices.
        this.ibo = gl.createBuffer();
        this.indexCount = this.indices.length;
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibo);  
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, this.indices, gl.STATIC_DRAW);

		//Set up vertices
        this.vbo = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
        gl.bufferData(gl.ARRAY_BUFFER, this.vertices, gl.STATIC_DRAW);
        gl.enableVertexAttribArray(vertexAttributeLocation);
        gl.vertexAttribPointer(vertexAttributeLocation, 3, gl.FLOAT, false, 0, 0);
        
        //Setup UV
        this.UVbo = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.UVbo);
        gl.bufferData(gl.ARRAY_BUFFER, this.uvs, gl.STATIC_DRAW);
        gl.enableVertexAttribArray(texCoordAttributeLocation);
        gl.vertexAttribPointer(texCoordAttributeLocation, 2, gl.FLOAT, true, 0, 0);

        //Setup normals
        if(normalAttributeLocation != -1) {
            this.nbo = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.nbo);
            gl.bufferData(gl.ARRAY_BUFFER, this.normals, gl.STATIC_DRAW);
            gl.enableVertexAttribArray(normalAttributeLocation);
            gl.vertexAttribPointer(normalAttributeLocation, 3, gl.FLOAT, false, 0, 0);
        }

        this.vao = gl.createVertexArray();
        gl.bindVertexArray(this.vao);

        //SET vertex buffer to VAO
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
        gl.enableVertexAttribArray(vertexAttributeLocation);
        gl.vertexAttribPointer(vertexAttributeLocation, 3, gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        
        //SET texture buffer to VAO
        gl.bindBuffer(gl.ARRAY_BUFFER, this.UVbo);
        gl.enableVertexAttribArray(texCoordAttributeLocation);
        gl.vertexAttribPointer(texCoordAttributeLocation, 2, gl.FLOAT, true, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        //SET normals buffer to VAO
        if(this.nbo) {
            gl.bindBuffer(gl.ARRAY_BUFFER, this.nbo);
            gl.enableVertexAttribArray(normalAttributeLocation);
            gl.vertexAttribPointer(normalAttributeLocation, 3, gl.HALF_FLOAT, false, 0, 0);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
        }

        //SET indices buffer to VAO
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibo);

        gl.bindVertexArray(null);
    }

    
}
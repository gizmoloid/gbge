import Vector3f from "./Vector3f";
import Matrix4f from "./Matrix4f";

/**
 * Transform class
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */

export default class Transform {

    private translation: Vector3f;
    private rotation: Vector3f;
    private scale: Vector3f;

    constructor() {

        this.translation = new Vector3f(0, 0, 0);
        this.rotation = new Vector3f(0, 0, 0);
        this.scale = new Vector3f(1, 1, 1);
    }

    public getTransformation(): Matrix4f {
        
        const translationMatrix: Matrix4f = new Matrix4f().translate( this.translation.getX(),
                                                                      this.translation.getY(),
                                                                      this.translation.getZ());
                             
        const rotationMatrix = new Matrix4f();
        rotationMatrix.rotate(this.rotation.getX(), 1, 0, 0);
        rotationMatrix.rotate(this.rotation.getY(), 0, 1, 0);
        rotationMatrix.rotate(this.rotation.getZ(), 0, 0, 1);
                                                                        
        const scaleMatrix: Matrix4f = new Matrix4f().scale( this.scale.getX(),
                                                            this.scale.getY(),
                                                            this.scale.getZ());                                                                    

                                                                
        // TRS order !important!
        return translationMatrix.multipy(rotationMatrix.multipy(scaleMatrix));
    }


    //GETTERS / SETTERS
    public getTranslation(): Vector3f {
        return this.translation;
    }
    public setTranslation(x: number, y: number, z: number): void {
        this.translation = new Vector3f(x, y, z);
    }
    public setTranslationVec(translation: Vector3f): void {
        this.translation = translation;
    }
    public getRotation(): Vector3f {
        return this.rotation;
    }
    public setRotationVec(rotation: Vector3f): void {
        this.rotation = rotation;
    }
    public setRotation(x: number, y: number, z: number): void {
        this.rotation = new Vector3f(x, y, z);
    }
    public getScale(): Vector3f {
        return this.scale;
    }
    public setScaleVec(scale: Vector3f): void {
        this.scale = scale;
    }
    public setScale(x: number, y: number, z: number): void {
        this.scale = new Vector3f(x, y, z);
    }
}
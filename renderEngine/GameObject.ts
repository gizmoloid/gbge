import Mesh from "./Mesh";
import Material from "./Material";
import Matrix4f from "./Matrix4f";
import Transform from "./Transform";
import BaseLight from "./BaseLight";
import Camera3D from "./Camera3D";
import Shader from "../shaders/Shader";
import BasicShader from "../shaders/BasicShader";
import PhongShader from "../shaders/PhongShader";
import DirectionalLight from "./DirectionalLight";

/**
 * Game Object class
 * @author Bruno Perry (brunoperry@gmail.com)
 */

export default class GameObject {

    private transform : Transform;
    private mesh : Mesh;
    private shader : PhongShader;
    private material : Material;

    constructor(mesh : Mesh, shader : Shader, material : Material) {

        this.mesh = mesh;
        this.shader = shader as PhongShader;
        this.material = material;

        this.transform = new Transform();
    }

    public update(tick:number) : void {

        const sin: number = Math.sin(tick);
        let r = sin * 180;

        this.transform.setTranslation(0,0,0);
        this.transform.setRotation(0,r,0);
        this.transform.setScale(1,1,1);
    }

    public render(camera: Camera3D, light: DirectionalLight) : void {

        const modelMatrix: Matrix4f        = this.transform.getTransformation();
        const viewMatrix: Matrix4f         = camera.getViewMatrix();
        const projectionMatrix: Matrix4f   = camera.getProjection();

        const modelToView: Matrix4f = viewMatrix.multipy(modelMatrix);
        const mvp: Matrix4f = projectionMatrix.multipy(modelToView);

        this.shader.setDirectionalLight(light);
        this.shader.update(this.transform.getTransformation(), mvp, this.material);

        this.mesh.draw();
    }

    public getTransform():Transform {
        return this.transform;
    }

    public getShader(): Shader {
        return this.shader;
    }
}
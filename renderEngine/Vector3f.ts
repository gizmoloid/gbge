import MathUtil from "../utils/MathUtil";
import Quaternion from "./Quaternion";

/**
 * Class for the Vector3f
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */

export default class Vector3f {

    public elements : Float32Array;

    /**
     * Constructor of Vector3
     */
    constructor(x : number = 0, y : number = 0, z : number = 0) {

        this.elements = new Float32Array(3);
        this.elements[0] = x;
        this.elements[1] = y;
        this.elements[2] = z;
    }

    public addVec(vec : Vector3f) : Vector3f {

        return new Vector3f(this.elements[0] + vec.getX(),
                            this.elements[1] + vec.getY(),
                            this.elements[2] + vec.getZ());
    }

    public subVec(vec : Vector3f) : Vector3f {

        return new Vector3f(
            this.getX() - vec.getX(),
            this.getY() - vec.getY(),
            this.getZ() - vec.getZ()
        )
    }

    public copy() : Vector3f {

        return new Vector3f(
            this.elements[0],
            this.elements[1],
            this.elements[2]
        )
    }

    public mulNum(num : number) : Vector3f {

        return new Vector3f(this.elements[0] * num,
                            this.elements[1] * num,
                            this.elements[2] * num);
    }

    public divVec(r : Vector3f) : Vector3f {

		return new Vector3f(
            this.elements[0] / r.getX(),
            this.elements[1] / r.getY(),
            this.elements[2] / r.getZ()
        );
	}
	
	public divNum(r : number) : Vector3f {

        return new Vector3f(
            this.elements[0] / r,
            this.elements[1] / r,
            this.elements[2] / r
        );
    }
    
	public abs() : Vector3f {

		return new Vector3f(
            Math.abs(this.elements[0]),
            Math.abs(this.elements[1]),
            Math.abs(this.elements[2])
        );
	}

    public cross(r : Vector3f) : Vector3f {

        const e = this.elements;

        const x_ = e[1] * r.getZ() - e[2] * r.getY();
        const y_ = e[2] * r.getX() - e[0] * r.getZ();
        const z_ = e[0] * r.getY() - e[1] * r.getX();

        return new Vector3f(x_, y_, z_);
    }

    public rotate(angle : number, axis : Vector3f) : Vector3f {

        const sinHalfAngle = Math.sin(MathUtil.toRadians(angle / 2));
        const cosHalfAngle = Math.cos(MathUtil.toRadians(angle / 2));

        const rX : number = axis.getX() * sinHalfAngle;
        const rY : number = axis.getY() * sinHalfAngle;
        const rZ : number = axis.getZ() * sinHalfAngle;
        const rW : number = cosHalfAngle;

        const rotation : Quaternion = new Quaternion(rX, rY, rZ, rW);
        const conjugate : Quaternion = rotation.conjugate();

        const w : Quaternion = rotation.mulVec(this).mulQuat(conjugate);

        this.elements[0] = w.getX();
        this.elements[1] = w.getY();
        this.elements[2] = w.getZ();

        return this;
    }

    /*
    * Normalize.
    * @return this
    */
    public normalized() : Vector3f {

        const length = this.length();
        const v = this.elements;
        return new Vector3f(
            v[0] / length,
            v[1] / length,
            v[2] / length
        );
    }

    public negate(): Vector3f {

        const v = this.elements;
        return new Vector3f(-v[0], -v[1], -v[2]);
    }

    public length() : number {
        let v = this.elements;
		return (Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]));
	}

    public getX() : number {
        return this.elements[0];
    }
    public setX(x : number) : void {
        this.elements[0] = x;
    }
    
    public getY() : number {
        return this.elements[1];
    }
    public setY(y : number) : void {
        this.elements[1] = y;
    }
    
    public getZ() : number {
        return this.elements[2];
    }
    public setZ(z : number) : void {
        this.elements[2] = z;
    }

    public static up():Vector3f {
        return new Vector3f(0, 1, 0);
    }
}
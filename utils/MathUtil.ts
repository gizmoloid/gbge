export default class MathUtil {

    public static toRadians(degrees: number): number {
        return degrees * Math.PI / 180;
    }

    public static createFPSCounterElement(): HTMLElement {

        const res = document.createElement("p");
        res.style.position = "absolute";
        res.style.top = "0";
        res.style.left = "0";
        res.style.zIndex = "9999";
        res.style.padding = "0";
        res.style.margin = "0";

        document.body.appendChild(res);

        return res;
    }

    
    public static indicesToInt32Array(indices: Array<number>): Int32Array {
        return new Int32Array(indices);;
    }

    public static base64ArrayBuffer(arrayBuffer : ArrayBuffer) : string {

        let base64 : string    = "";
        const encodings : string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
      
        const bytes : Uint8Array = new Uint8Array(arrayBuffer)
        const byteLength : number   = bytes.byteLength
        const byteRemainder : number = byteLength % 3
        const mainLength : number    = byteLength - byteRemainder
      
        let a : number;
        let b : number;
        let c : number;
        let d : number;
        let chunk : number;
      
        // Main loop deals with bytes in chunks of 3
        for (let i : number = 0; i < mainLength; i = i + 3) {
          // Combine the three bytes into a single integer
          chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2]
      
          // Use bitmasks to extract 6-bit segments from the triplet
          a = (chunk & 16515072) >> 18 // 16515072 = (2^6 - 1) << 18
          b = (chunk & 258048)   >> 12 // 258048   = (2^6 - 1) << 12
          c = (chunk & 4032)     >>  6 // 4032     = (2^6 - 1) << 6
          d = chunk & 63               // 63       = 2^6 - 1
      
          // Convert the raw binary segments to the appropriate ASCII encoding
          base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
        }
      
        // Deal with the remaining bytes and padding
        if (byteRemainder == 1) {
          chunk = bytes[mainLength]
      
          a = (chunk & 252) >> 2 // 252 = (2^6 - 1) << 2
      
          // Set the 4 least significant bits to zero
          b = (chunk & 3)   << 4 // 3   = 2^2 - 1
      
          base64 += encodings[a] + encodings[b] + "==";
        } else if (byteRemainder == 2) {
          chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1]
      
          a = (chunk & 64512) >> 10 // 64512 = (2^6 - 1) << 10
          b = (chunk & 1008)  >>  4 // 1008  = (2^6 - 1) << 4
      
          // Set the 2 least significant bits to zero
          c = (chunk & 15)    <<  2 // 15    = 2^4 - 1
      
          base64 += encodings[a] + encodings[b] + encodings[c] + '='
        }
        
        return base64;
      }

    public static removeEmptyStrings(data: Array<string>): Array<string> {

        let result: Array<string> = new Array<string>();

        for(let i: number = 0; i < data.length; i++) {

            if(data[i] !== "") {
                result.push(data[i]);
            }
        }

        return result;
    }

    public static async loadJSONFile(url: string): Promise<JSON> {
        let response = await fetch(url);
        return await response.json();
    }
    
    public static async loadASCIIFile(url: string): Promise<JSON> {
        let response = await fetch(url);
        return await response.json();
    }
}

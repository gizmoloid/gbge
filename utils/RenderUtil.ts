import Vector2f from "../renderEngine/Vector2f";
import Vector3f from "../renderEngine/Vector3f";

/**
 * Class RenderUtil
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */


export default class RenderUtil {

    public static gl: WebGL2RenderingContext;

    public static clearScreen(): void {

        // TODO: Stencil Buffer
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
    }

    public static setClearColor(color : Vector3f) : void {

        this.gl.clearColor(color.getX(), color.getY(), color.getZ(), 1.0);
    }

    public static getGLVersion(): string {

        return this.gl.getParameter(this.gl.VERSION);
    }

    public static unbindTextures() :void {
        this.gl.bindTexture(this.gl.TEXTURE_2D, 0);
    }

    public static initGraphics(canvas: HTMLCanvasElement): void {

        try {
            this.gl = canvas.getContext("webgl2") as WebGL2RenderingContext;
        } catch (error) {
            throw new Error("WebGL error: " + error);
        };

        this.gl.clearColor(0, 0, 0, 0);

        this.gl.frontFace(this.gl.CCW);
        this.gl.cullFace(this.gl.BACK);
        this.gl.enable(this.gl.CULL_FACE);
        this.gl.enable(this.gl.DEPTH_TEST);
    }
}
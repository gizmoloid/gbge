import Shader from "./Shader";
import RenderUtil from "../utils/RenderUtil";
import Vector3f from "../renderEngine/Vector3f";
import ResourceLoader from "../loaders/ResourceLoader";
import Matrix4f from "../renderEngine/Matrix4f";
import Material from "../renderEngine/Material";
import DirectionalLight from "../renderEngine/DirectionalLight";
import BaseLight from "../renderEngine/BaseLight";

/**
 * Phong Shader class
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */

export default class PhongShader extends Shader {

    private ambientLight: Vector3f = new Vector3f(0.1,0.1,0.1);

    private directionalLight: DirectionalLight = new DirectionalLight(
        new BaseLight(new Vector3f(1,1,1), 0),
        new Vector3f(0,0,0)
    );

    constructor() {
        super();
        
        this.setShaders(
            ResourceLoader.loadShader("phongVertex.glsl"),
            ResourceLoader.loadShader("phongFragment.glsl")
        );

        this.addUniform("worldMatrix");
        this.addUniform("mvp");
        this.addUniform("baseColor");
        this.addUniform("ambientLight");

        this.addUniform("directionalLight.base.color");
        this.addUniform("directionalLight.base.intensity");
        this.addUniform("directionalLight.direction");
    }

    public update(worldMatrix: Matrix4f, mvp: Matrix4f, material: Material) {
        super.update(worldMatrix, mvp, material);

        if(material.getTexture() !== null) {
            material.getTexture().bind();
        } else {
            RenderUtil.unbindTextures();
        }
        this.setUniform("mvp", mvp);
        this.setUniform("worldMatrix", worldMatrix);
        this.setUniformVec("baseColor", material.getColor());
        this.setUniformVec("ambientLight", this.ambientLight);

        this.setUniformDirLight("directionalLight", this.directionalLight);
    }

    private setUniformBaseLight(uniformName: string, baseLight: BaseLight): void {
        this.setUniformVec(uniformName + ".color", baseLight.getColor());
        this.setUniformf(uniformName + ".intensity", baseLight.getIntensity());
    }
    private setUniformDirLight(uniformName: string, directionalLight: DirectionalLight): void {
        this.setUniformBaseLight(uniformName + ".base", directionalLight.getBase());
        this.setUniformVec(uniformName + ".direction", directionalLight.getDirection());
    }

    //GETTERS/SETTERS
    public getAmbientLight(): Vector3f {
        return this.ambientLight;
    }
    public setAmbientLight(ambientLight: Vector3f): void {
        this.ambientLight = ambientLight;
    }
    public getDirectionalLight(): DirectionalLight {
        return this.directionalLight;
    }
    public setDirectionalLight(directionalLight: DirectionalLight): void {
        this.directionalLight = directionalLight;
    }
}
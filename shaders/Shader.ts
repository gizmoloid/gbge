import RenderUtil from "../utils/RenderUtil";
import Matrix4f from "../renderEngine/Matrix4f";
import Material from "../renderEngine/Material";
import BaseLight from "../renderEngine/BaseLight";
import Vector3f from "../renderEngine/Vector3f";

/**
 * Basic Shader class
 * 
 * @author Bruno Perry (brunoperry@gmail.com)
 */

export default class Shader {

    private program: WebGLProgram | null;
    protected programInfo : any;
    protected uniforms: Map<string, WebGLUniformLocation>;

    constructor() {

        this.program = RenderUtil.gl.createProgram();
        this.uniforms = new Map<string, WebGLUniformLocation>();

        if(RenderUtil.gl.getProgramParameter(this.program, RenderUtil.gl.LINK_STATUS)) {
            throw new Error("Error memory valid location not valid!");
        }
    }

    public bind(): void {
        RenderUtil.gl.useProgram(this.program);
    }

    public setShaders(vertexShader : string, fragShader : string) : void {

        const gl : WebGLRenderingContext  = RenderUtil.gl;
        this.addProgram(vertexShader, gl.VERTEX_SHADER);
        this.addProgram(fragShader, gl.FRAGMENT_SHADER);

        gl.linkProgram(this.program);
        if(!gl.getProgramParameter(this.program, gl.LINK_STATUS)) {
            throw new Error("Error compile program link status");
        }

        gl.validateProgram(this.program);
        if(!gl.getProgramParameter(this.program, gl.VALIDATE_STATUS)){
            throw new Error("Error compile program validate status");
        }
    }

    protected addUniform(uniformName: string): void {
        this.uniforms.set(uniformName, RenderUtil.gl.getUniformLocation(this.program, uniformName) as WebGLUniformLocation)
    }

    private addProgram(source: string, type: number): void {

        const gl : WebGLRenderingContext  = RenderUtil.gl;
        const shader: WebGLShader | null = gl.createShader(type);

        gl.shaderSource(shader, source);
        gl.compileShader(shader);

        if(!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            throw new Error("Error compiling shader program: \n" + source );
        }

        gl.attachShader(this.program, shader);
    }

    public update(worldMatrix: Matrix4f, mvp: Matrix4f, material: Material): void {
        this.bind();
    }
    public getProgram() : WebGLProgram | null {
        return this.program;
    }
    //UNIFORMS RELATED
    public setUniformi(uniformName: string, value: number): void {
        RenderUtil.gl.uniform1i(this.uniforms.get(uniformName) as WebGLUniformLocation, value);
    }
    public setUniformf(uniformName: string, value: number): void {
        RenderUtil.gl.uniform1f(this.uniforms.get(uniformName) as WebGLUniformLocation, value);
    }
    public setUniformVec(uniformName: string, value: Vector3f): void {
        RenderUtil.gl.uniform3f(this.uniforms.get(uniformName) as WebGLUniformLocation, value.getX(), value.getY(), value.getZ());
    }
    public setUniform(uniformName: string, value: Matrix4f): void {

        const uniformN = this.uniforms.get(uniformName) as WebGLUniformLocation;
        RenderUtil.gl.uniformMatrix4fv(uniformN, false, value.elements);
    }
}

export enum ShaderType {
    BASIC = "basic",
    PHONG = "phong",
    CUSTOM = "custom"
}
import Shader from "./Shader";
import ResourceLoader from "../loaders/ResourceLoader";
import RenderUtil from "../utils/RenderUtil";
import Matrix4f from "../renderEngine/Matrix4f";
import Material from "../renderEngine/Material";

export default class BasicShader extends Shader {

    constructor() {
        super();
        
        this.setShaders(
            ResourceLoader.loadShader("basicVertex.glsl"),
            ResourceLoader.loadShader("basicFragment.glsl")
        );

        this.addUniform("worldMatrix");
        this.addUniform("baseColor");
        this.addUniform("sampled");
    }

    public update(worldMatrix: Matrix4f, mvp: Matrix4f, material: Material) {

        super.update(worldMatrix, mvp, material);

        if(material.getTexture() !== null) {
            material.getTexture().bind();
        } else {
            RenderUtil.unbindTextures();
        }
        this.setUniform("worldMatrix", mvp);
        this.setUniformVec("baseColor", material.getColor());
    }
}